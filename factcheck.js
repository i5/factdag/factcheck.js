const checksums = require("./lib/crypto/checksums");
const signatures = require("./lib/crypto/signatures");
const pki = require("./lib/crypto/pki");
const config = require("./lib/config");

module.exports.server = require("./lib/server");
module.exports.client = require("./lib/client");
module.exports.facts = require("./lib/facts");
module.exports.verify = require("./lib/verifier").verify;
module.exports.setConfig = (newConfig) => {
  config.set(newConfig);
};
module.exports.init = async () => {
  return Promise.all([checksums.init(), signatures.init(), pki.init()]);
};

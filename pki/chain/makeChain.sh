#!/bin/bash

cd "$(dirname "$0")"

mkdir -p certs

n=$1

openssl genrsa -out certs/cert-1-key.pem 2048
openssl req -new -sha256 -key certs/cert-1-key.pem -subj "/O=Example chain cert 1/CN=localhost" -out certs/cert-1.csr
openssl x509 -req -in certs/cert-1.csr \
	-extfile ext.cnf -extensions v3_intermediate_ca \
	-CA ../ca/ca-cert.pem -CAcreateserial \
	-CAkey ../ca/ca-priv.pem -out certs/cert-1.pem -days 365 -sha256


i=2
while [ "$i" -le "$n" ]; do
	openssl genrsa -out certs/cert-$i-key.pem 2048
	openssl req -new -sha256 -key certs/cert-$i-key.pem -subj "/O=Example chain cert $i/CN=localhost" \
		-out certs/cert-$i.csr
	openssl x509 -req -in certs/cert-$i.csr \
		-extfile ext.cnf -extensions v3_intermediate_ca \
		-CA certs/cert-$((i-1)).pem -CAcreateserial \
	       	-CAkey certs/cert-$((i-1))-key.pem -out certs/cert-$i.pem -days 365 -sha256
	i=$(($i + 1))
done

echo -n "" > chain.pem
i=$n
while [ "$i" -ge 1 ]; do
	cat certs/cert-$i.pem >> chain.pem
	i=$(($i - 1))
done

openssl crl2pkcs7 -nocrl -certfile chain.pem -out chain-pkcs7.pem
cp certs/cert-$n-key.pem ./priv.pem

if find ./certs -type f | grep -q -v cert-[1-$n]
then
	find ./certs -type f | grep -v cert-[1-$n] | xargs rm
fi

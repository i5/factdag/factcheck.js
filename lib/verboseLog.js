const config = require("./config");

function _vl(moduleName, str) {
  if (config.get().verbose) {
    let dateString = new Date().toISOString();
    console.log(
      `[factcheck@%s] \x1b[36m%s\x1b[0m %s${dateString} verbose ` +
        `${moduleName}: ${str}`
    );
  }
}

module.exports = _vl;

//TODO: this is called on require, not on init! It is thus printed before the config is set, and thus before logging is potentially disabled.
_vl("Info: Verbose logging enabled!");

const conn = require("./conn");
const { Contract } = require("./contracts");
const schemas = require("./schemas");
const { makeR1, makeR2, makeErrorMessage } = require("./messages");
const vl = require("./verboseLog");
const { getCACertificates } = require("./crypto/pki");
const config = require("./config");
const facts = require("./facts");

const http = require("http");
const https = require("https");

const _moduleName = "client.js";

/**
 * Central client api function to request DTCs.
 * Combines TCP, TLS, HTTP and HTTPS modes.
 *
 * @param {*} factIDs The factIDs to request the DTC for
 * @param {*} options Options object
 */
module.exports.requestContract = (
  factIDs,
  options = {
    mode: "raw", // protocol mode, "raw"/"tcp", "tls", "http", "https"
    port: 8000, // port on which the server interface runs
    host: "localhost", // host of the server
    path: "/factcheckAPI", // path of the HTTP/HTTPS factcheck interface (only HTTP/HTTPS)
    authID, // ID (IRI) of the own authority
    customContent: {}, // The receiver's custom content, will be added to the DTC
    onError: (error) => {}, // onError callback, is called with a string error
    onContract: (contract) => {}, // onContract callback, will be called with the complete contract if successful
    evalPortCallback: (port) => {}, // Used to extract the client port for distinction in evaluation.
    supportStatefulMode: true, // Define whether the stateful mode is supported on client side.
  }
) => {
  switch (options.mode) {
    case "raw":
    case "tcp":
    case "tls":
      return _requestContractRaw(factIDs, options);
    case "http":
    case "https":
      return _requestContractHttp(factIDs, options);
    default:
      throw new Error(`Unrecognized client mode: ${options.mode}`);
  }
};

function _requestContractRaw(
  factIDs,
  options = {
    port: 8000,
    host: "localhost",
    authID,
    customContent: {},
    onError: (error) => {},
    onContract: (contract) => {},
    evalPortCallback: (port) => {}, // only eval! (hotfix)
    supportStatefulMode: false,
  }
) {
  let {
    port,
    host,
    authID,
    onError,
    onContract,
    customContent,
    evalPortCallback,
    supportStatefulMode,
  } = options;
  port = port || 8080;
  host = host || "localhost";
  customContent = customContent || {};
  onError = onError || ((error) => {});
  onContract = onContract || ((contract) => {});
  evalPortCallback = evalPortCallback || ((port) => {});
  authID = typeof authID === "string" ? authID : undefined;
  supportStatefulMode = supportStatefulMode ? true : false;

  let context = {
    onError: onError,
    onContract: onContract,
    writeMessage: (message) => {},
    close: () => {},
    waitingForS1: false,
    authID,
    supportStatefulMode,
  };

  vl(_moduleName, "Creating connection");
  // Use the conn module to create a TCP/TLS connection
  const connection = conn.createClientConnection(
    port,
    host,
    handleConnection,
    (msg) => handleMessage(msg, context),
    onError,
    evalPortCallback // only eval! (hotfix)
  );

  context.writeMessage = connection.writeMessage;
  context.close = connection.close;

  function handleConnection() {
    vl(_moduleName, "Established connection with server, sending R1");
    context.waitingForS1 = true;
    const [message, contract] = makeR1(
      factIDs,
      customContent,
      authID,
      supportStatefulMode
    );
    // Save contract state if stateful mode is supported
    if (supportStatefulMode) {
      context.contractState = contract;
    }
    connection.writeMessage(message);
  }
}

function _requestContractHttp(
  factIDs,
  options = {
    mode: "",
    port: 8080,
    host: "",
    path: "/",
    authID,
    customContent: {},
    onError: (error) => {},
    onContract: (contract) => {},
    evalPortCallback: (port) => {}, // only eval!
    supportStatefulMode: false,
  }
) {
  let {
    mode,
    port,
    host,
    path,
    authID,
    customContent,
    onError,
    onContract,
    evalPortCallback,
    supportStatefulMode,
  } = options;

  mode = mode || "http";
  port = port || 8080;
  host = host || "localhost";
  path = path || "/";
  customContent = customContent || {};
  onError = onError || ((error) => {});
  onContract = onContract || ((contract) => {});
  evalPortCallback = evalPortCallback || ((error) => {});
  authID = typeof authID === "string" ? authID : undefined;
  supportStatefulMode = supportStatefulMode ? true : false;

  let context = {
    writeMessage,
    onError,
    onContract,
    close: () => {},
    waitingForS1: false,
    authID,
    supportStatefulMode,
  };

  // load http or https module
  let _module;
  switch (mode) {
    case "http":
      _module = http;
      break;
    case "https":
      _module = https;
      break;
    default:
      throw new Error(`Client mode is not http/https, got ${mode} instead!`);
  }

  // If independent requests are needed (e.g. for evaluation),
  // a new agent has to be created for each run (else the TLS handshake
  // is omitted between runs => not independent)
  // However, agents cannot simply be disabled, as then the client has to
  // do two TLS handshakes in one run (one for r1/s1, the other for r2/ACK)
  let agent = config.get().independentAgents
    ? new _module.Agent()
    : _module.globalAgent;

  let cache = new conn.ClientCache(config.get().cacheLifetime || 2000);

  const [r1Message, contract] = makeR1(
    factIDs,
    customContent,
    authID,
    supportStatefulMode
  );
  // Save contract state if stateful mode is supported
  if (supportStatefulMode) {
    context.contractState = contract;
  }
  context.waitingForS1 = true;
  writeMessage(r1Message);

  function writeMessage(message) {
    const messageBody = JSON.stringify(message);

    // Use the NodeJS http/https module to build the request
    const req = _module.request(
      {
        host: host,
        port: port,
        path: path,
        method: "POST",
        headers: {
          "Content-Type": "application/json", // JSON messages
          "Content-Length": Buffer.byteLength(messageBody),
        },
        ca: getCACertificates("pem"),
        agent,
      },
      (res) => {
        res.setEncoding("utf8");
        if (!res.headers["content-type"] === "application/json") {
          onError(
            `Content-Type header of response was not application/json!
          Instead got ${res.headers["content-type"]}
          Are you sure you are using the correct host, path and port?`
          );
        }
        res.on("data", (data) => {
          try {
            const parsedMsg = JSON.parse(data);
            handleMessage(parsedMsg, context);
          } catch (err) {
            // Duplicate of conn.js->createClientConnection (TODO: refactor)
            if (err instanceof SyntaxError) {
              // Invalid JSON
              if (config.get().enableCaching) {
                // Cache is enabled
                // Extra check: If cache is empty and message does not start with a "{", reject
                if (data.charAt(0) !== "{" && cache.get().length === 0) {
                  context.writeMessage(
                    makeErrorMessage("D: Message was not valid JSON!")
                  );
                } else {
                  // Assume fragmented message. (TODO: When to throw the error?)
                  // 1: Try to assemble messages in the cache
                  let cacheData = cache.get();
                  cacheData.push(data);
                  let assembledMsg = cacheData.join("");
                  try {
                    let parsedMsg = JSON.parse(assembledMsg.trim());
                    cache.clear();
                    handleMessage(parsedMsg, context);
                  } catch (err2) {
                    if (err2 instanceof SyntaxError) {
                      // Assembled message still is not valid JSON. Put data into cache.
                      cache.add(data);
                    } else {
                      throw err2;
                    }
                  }
                }
              } else {
                // Cache is disabled. Message was not valid JSON. (May be caused by message fragmentation)
                context.writeMessage(
                  makeErrorMessage("A: Message was not valid JSON!")
                );
                onError("B: Received message was not valid JSON!");
              }
            } else {
              throw err;
            }
          }
        });
      }
    );

    req.on("socket", (socket) => {
      socket.on("connect", () => {
        evalPortCallback(socket.localPort);
      });
    });

    req.on("error", (e) => {
      console.error(`Problem with request: ${e.message}`);
    });
    req.write(messageBody);
    req.end();
  }
}

function handleMessage(message, context) {
  // This is the central message handler, used in all modes.
  // The message is an already parsed JSON message.

  vl(_moduleName, "Received new message");

  // Determine message type using JSON schemas.
  // For a client, only S1 and error messages are relevant.
  if (
    schemas.s1.validate(message) &&
    (schemas.contract.s1.validate(message.contract) ||
      (message.stateKey &&
        schemas.contract.s1Stateful.validate(message.contract)))
  ) {
    try {
      if (!context.waitingForS1) {
        // This is especially relevant for non-HTTP versions, as the server does not
        // respond to a request in this case, and thus can send arbitrary messages.
        throw new MessageError("Received S1, but did not expect it");
      }
      // Message is S1 and the client expected it
      vl(_moduleName, "Message was expected S1");
      context.waitingForS1 = false;

      let contract;
      // Only stateful mode. Contract is assembled with the local state cache.
      if (message.stateKey) {
        if (!context.supportStatefulMode) {
          throw new MessageError(
            "Received message indicated stateful mode while " +
              "stateful mode is not supported client-side"
          );
        }
        contract = context.contractState;
        contract.updateWithMessage(message);
        context.contractStateKey = message.stateKey;
      } else {
        contract = Contract.fromMessage(message);
      }
      let checks = [
        contract.verifySenderCertificate(), // Verify the sender certificate
        contract.verifySenderSig(), // Verify the SenderSig
        contract.timestampIsRecent(), // Check timestamp recency
      ];
      // Optional checksum verification for independence of data transmission.
      // Needs to be enabled to prevent server-side checksum forgery during DTC generation.
      if (config.get().clientChecksumVerification) {
        checks.push(verifyChecksums(contract));
      }
      // Execute verifications
      Promise.all(checks).then((results) => {
        if (results.every((result) => result)) {
          vl(_moduleName, "All client-side checks passed. Accepting contract");
          vl(_moduleName, "Sending R2");
          // R2 message as answer to S1, DTC is complete now
          context.writeMessage(makeR2(contract, context.contractStateKey));
          context.onContract(contract);
          context.close();
        } else {
          throw new MessageError(
            "Could not verify senderCertficate or sender's cert!"
          );
        }
      });
    } catch (err) {
      // Only catch custom MessageError instances here
      if (err instanceof MessageError) {
        context.onError(err);
        context.writeMessage(makeErrorMessage(err));
      } else {
        throw err;
      }
    }
  } else if (schemas.error.validate(message)) {
    // Message received from server was an error message
    context.onError("Received error from server: " + message.errorMessage);
  } else {
    console.log("Weird message: " + JSON.stringify(message));
    context.writeMessage(makeErrorMessage("Unrecognized message format!"));
  }
}

async function verifyChecksums(contract) {
  try {
    let factData = {};
    await Promise.all(
      contract.getContent("internal").facts.map(async (factInContract) => {
        let { factID } = factInContract;
        let fact = await facts.getFactByID(factID)[0];
        if (fact === undefined || fact === "") {
          return false;
        } else {
          factData[factID] = fact;
        }
      })
    );
    let verifyChecksumsRes = contract.verifyChecksums(factData);
    if (!verifyChecksumsRes.result) {
      console.log("Checksum verification failed:");
      console.log(JSON.stringify(verifyChecksumsRes, null, 2));
      return false;
    }
    return true;
  } catch (e) {
    console.log(e);
  }
  return false;
}

class MessageError extends Error {
  constructor(message) {
    super(message);
    this.name = "MessageError";
  }
}

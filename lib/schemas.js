const ajv = new require("ajv")();

const schemas = {
  r1: { bla: "bla" },
  r2: {},
  s1: {},
  error: {},
  contract: {
    r1: {},
    r2: {},
    s1: {},
    s1Stateful: {},
    r2Stateful: {},
  },
};

async function _initSchemas() {
  let fs = require("fs");
  schemas.r1.schema = require("./schemas/r1-schema.json");
  schemas.r2.schema = require("./schemas/r2-schema.json");
  schemas.s1.schema = require("./schemas/s1-schema.json");
  schemas.error = require("./schemas/error-schema.json");
  schemas.contract.r1.schema = require("./schemas/r1-contract-schema.json");
  schemas.contract.r2.schema = require("./schemas/r2-contract-schema.json");
  schemas.contract.s1.schema = require("./schemas/s1-contract-schema.json");
  schemas.contract.s1Stateful.schema = require("./schemas/s1-contract-schema-stateful.json");
  schemas.contract.r2Stateful.schema = require("./schemas/r2-contract-schema-stateful.json");
}

function _compileSchemas() {
  schemas.r1.validate = ajv.compile(schemas.r1.schema);
  schemas.r2.validate = ajv.compile(schemas.r2.schema);
  schemas.s1.validate = ajv.compile(schemas.s1.schema);
  schemas.error.validate = ajv.compile(schemas.error);
  schemas.contract.r1.validate = ajv.compile(schemas.contract.r1.schema);
  schemas.contract.r2.validate = ajv.compile(schemas.contract.r2.schema);
  schemas.contract.s1.validate = ajv.compile(schemas.contract.s1.schema);
  schemas.contract.s1Stateful.validate = ajv.compile(
    schemas.contract.s1Stateful.schema
  );
  schemas.contract.r2Stateful.validate = ajv.compile(
    schemas.contract.r2Stateful.schema
  );
}

async function init() {
  await _initSchemas();
  _compileSchemas();
}

module.exports = schemas;
module.exports.ready = init();

const factsModule = require("./facts");
const { Contract } = require("./contracts");
const schemas = require("./schemas");

async function _verify(
  contract,
  options = {
    getFactMethod: "internal",
    getFactByID: function () {},
    facts: {},
    contractFormat: "internal",
    ignoreCertificates: false,
    ignoreSignatures: false,
    ignoreChecksums: false,
  }
) {
  let {
    getFactMethod,
    getFactByID,
    facts,
    contractFormat,
    ignoreCertificates,
    ignoreSignatures,
    ignoreChecksums,
  } = options;
  getFactMethod = getFactMethod || "internal";

  getFactByID = getFactByID || function () {};
  facts = facts || {};
  contractFormat = contractFormat || "internal";
  ignoreCertificates = ignoreCertificates === true;
  ignoreSignatures = ignoreSignatures === true;
  ignoreChecksums = ignoreChecksums === true;

  let res = { result: true, errors: [] };

  const _contract = await parseContract(contract, contractFormat);

  // STEP 1: Verify IDs (certificates)
  if (!ignoreCertificates) {
    if (!(await _contract.verifySenderCertificate())) {
      res.result = false;
      res.errors.push("Sender certificate could not be verified");
    }

    if (!(await _contract.verifyReceiverCertificate())) {
      res.result = false;
      res.errors.push("Receiver certificate could not be verified");
    }
  }

  // STEP 2: Verify signatures
  if (!ignoreSignatures) {
    if (!(await _contract.verifySenderSig())) {
      res.result = false;
      res.errors.push("Sender signature could not be verified");
    }

    if (!(await _contract.verifyReceiverSig())) {
      res.result = false;
      res.errors.push("Receiver signature could not be verified");
    }
  }

  // STEP 3: Verify fact checksums
  if (!ignoreChecksums) {
    function _getFactByID(factID) {}
    switch (getFactMethod.toLowerCase()) {
      case "internal":
        _getFactByID = (factID) => factsModule.getFactByID(factID)[0];
        break;
      case "custom":
        _getFactByID = getFactByID;
        break;
      case "direct":
        _getFactByID = (factID) => facts[factID];
        break;
      default:
        throw new Error("getFactMethod did not match any known method");
    }

    let factData = {};

    await Promise.all(
      _contract.getContent("internal").facts.map(async (factInContract) => {
        let { factID } = factInContract;
        let fact = await _getFactByID(factID);
        if (fact === undefined || fact === "") {
          res.result = false;
          res.errors.push(`Fact with ID ${factID} could not be retrieved`);
        } else {
          factData[factID] = fact;
        }
      })
    );

    let verifyChecksumsRes = _contract.verifyChecksums(factData);
    if (!verifyChecksumsRes.result) {
      res.result = false;
      verifyChecksumsRes.errors.forEach((err) => {
        res.errors.push(err);
      });
    }
  }

  return res;
}

async function parseContract(contract, format = "") {
  let internalContract;
  switch (format.toLowerCase()) {
    case "internal":
      if (!contract instanceof Contract) {
        throw new TypeError(
          "Argument contract was not of the expected Type (internal)!"
        );
      }
      internalContract = contract;
      break;
    case "object":
      if (typeof contract !== "object") {
        throw new TypeError(
          "Argument contract was not of the expected Type (object)!"
        );
      }
      internalContract = Contract.fromObject(contract);
      break;
    case "json":
      if (typeof contract !== "string") {
        throw new TypeError(
          "Argument contract was not of the expected Type (string)!"
        );
      }
      internalContract = Contract.fromJSON(contract);
      break;
    case "jsonld":
      if (typeof contract !== "string" && typeof contract !== "object") {
        throw new TypeError(
          "Argument contract was not of the expected type (string|object)!"
        );
      }
      internalContract = await Contract.fromJSONLD(contract);
      break;
    default:
      throw new Error("Contract format did not match any known format");
  }
  if (!schemas.contract.r2.validate(internalContract.getContent("copy"))) {
    throw new Error(
      "Passed contract did not comply to the schema of a complete contract!"
    );
  }
  return internalContract;
}

module.exports.verify = _verify;

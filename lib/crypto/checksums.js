//const _sodium = require("libsodium-wrappers-sumo");
const { md } = require("node-forge");

const _byteToHexArray = [];
//init _byteToHexArray
for (let n = 0; n <= 0xff; ++n) {
  const hexOctet = n.toString(16).padStart(2, "0");
  _byteToHexArray.push(hexOctet);
}

async function init() {
  // wait for libsodium to load
  //await _sodium.ready;
}

function byteArrayToHexString(arrayBuffer) {
  //source: https://www.thetopsites.net/article/50767210.shtml
  const buff = new Uint8Array(arrayBuffer);
  const hexOctets = [];

  for (let i = 0; i < buff.length; ++i)
    hexOctets.push(_byteToHexArray[buff[i]]);

  return hexOctets.join("");
}

const _genericSum = (input, hashFunction) =>
  hashFunction.create().update(input).digest().toHex();

const md5sum = (input) => _genericSum(input, md.md5);

const sha256sum = (input) => _genericSum(input, md.sha256);

const sha384sum = (input) => _genericSum(input, md.sha384);

const sha512sum = (input) => _genericSum(input, md.sha512);

//const b2sum = (input) =>
//  byteArrayToHexString(_sodium.crypto_generichash(64, input, null));

function verify(alg, input, value) {
  switch (alg) {
    case "md5":
      return value === md5sum(input);
    case "sha256":
      return value === sha256sum(input);
    case "sha384":
      return value === sha384sum(input);
    case "sha512":
      return value === sha512sum(input);
  }
  return false;
}

module.exports = {
  md5sum: md5sum,
  sha256sum: sha256sum,
  sha384sum: sha384sum,
  sha512sum: sha512sum,
  supportedAlgs: ["sha256", "sha384", "sha512", "md5"],
  //b2sum: b2sum,
  verify: verify,
  init: init,
};

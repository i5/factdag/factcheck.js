const forge = require("node-forge");
const fs = require("fs");
const config = require("../config");
const path = require("path");
let caCertPems = [],
  caStore,
  certPem,
  ownCertChain,
  privateKeyPem;

module.exports.init = async () => {
  // load ca certs in caCertPath (as forge caStore)
  const caCertPath = config.get().pki.caCertPath;
  const caCertFiles = fs.readdirSync(caCertPath);
  let caCerts = [];
  caCertFiles.forEach((file) => {
    try {
      const filePath = path.join(caCertPath, file);
      let filePem = fs.readFileSync(filePath).toString();
      caCerts.push(forge.pki.certificateFromPem(filePem));
      //Push to array after forge, so that errors are alreexady caught
      caCertPems.push(filePem);
    } catch (err) {
      console.log("Skipped CA cert: " + file + " (" + err.message + ")");
    }
  });
  caStore = forge.pki.createCaStore(caCerts);

  // read own cert (as PEM)
  data = fs.readFileSync(config.get().pki.ownCert.path);
  certPem = data.toString();
  const certType = config.get().pki.ownCert.type;
  switch (certType.toLowerCase()) {
    case "x509-single":
    case "x509":
      ownCertChain = [forge.pki.certificateFromPem(certPem)];
      break;
    case "x509-pkcs7-chain":
    case "pkcs7":
      let chain = forge.pkcs7.messageFromPem(certPem).certificates;
      ownCertChain = chain;
      break;
    default:
      throw new Error("Unsupported certificate type in config: " + certType);
  }

  // read private key (as PEM)
  data = fs.readFileSync(config.get().pki.privateKey.path);
  privateKeyPem = data.toString();
};

module.exports.removePEMStructure = removePEMStructure = (pemFile) => {
  if (!(pemFile instanceof String)) {
    pemFile = pemFile.toString();
  }
  const lines = pemFile.split("\n");
  const filteredLines = lines.filter(
    (line) =>
      !/^-----BEGIN.*-----$/.test(line) && !/^-----END.*-----$/.test(line)
  );
  return filteredLines.join("");
};

module.exports.addPEMStructure = addPEMStructure = (certString, typeName) => {
  const lines = [];
  lines.push("-----BEGIN " + typeName.toUpperCase() + "-----");
  for (let i = 0; i < certString.length; i += 64) {
    lines.push(certString.substring(i, Math.min(i + 64), certString.length));
  }
  lines.push("-----END " + typeName.toUpperCase() + "-----\n");
  return lines.join("\n");
};

module.exports.getPublicKeyFromCertificate = (
  cert,
  type = "X509-single",
  format = "base64"
) => {
  let pem;
  let pemType = getPemTypeFromCertType(type);
  switch (format) {
    case "base64":
      pem = this.addPEMStructure(cert, pemType);
      break;
    case "PEM":
      pem = cert;
      break;
    default:
      throw new Error("Could not extract public key: Unknown format string!");
  }

  switch (type.toLocaleLowerCase()) {
    case "x509-single":
    case "x509":
      return forge.pki.certificateFromPem(pem).publicKey;
    case "x509-pkcs7-chain":
    case "pkcs7":
      let chain = forge.pkcs7.messageFromPem(pem).certificates;
      let lastCert = chain[0];
      return lastCert.publicKey;
    default:
      throw new Error(`Could not extract public key: Unknown type (${type})`);
  }
};

/*module.exports.parseEd25519PrivateKeyPem = (pem, cert) => {
  const msg = forge.pem.decode(pem)[0].body;
  const der = stringToByteArray(msg);
  const asn1 = forge.asn1.fromDer(der);
  const privKey = forge.pki.ed25519.privateKeyFromAsn1(asn1);
  //console.log(forge.pki.ed25519);

  const parsedCert = forge.pki.certificateFromPem(cert);

  return new Uint8Array(privKey.privateKeyBytes);
};*/

module.exports.verifyCertificate = (cert, type, format) => {
  let pemCert;
  let pemType = getPemTypeFromCertType(type);
  switch (format) {
    case "base64":
      pemCert = this.addPEMStructure(cert, pemType);
      break;
    case "pem":
      pemCert = cert;
      break;
    default:
      return false;
  }

  try {
    let certChain;
    switch (type.toLowerCase()) {
      case "x509":
      case "x509-single":
        certChain = [forge.pki.certificateFromPem(pemCert, true)];
        break;
      case "pkcs7":
      case "x509-pkcs7-chain":
        certChain = forge.pkcs7.messageFromPem(pemCert).certificates;
    }
    return forge.pki.verifyCertificateChain(caStore, certChain);
  } catch (e) {
    console.log(e);
  }
  return false;
};

module.exports.getCertificateChain = (format = "pem") => {
  switch (format.toLowerCase()) {
    case "pem":
      return ownCertChain
        .map((cert) => forge.pki.certificateToPem(cert))
        .join("");
    case "pem-array":
      return ownCertChain.map((cert) => forge.pki.certificateToPem(cert));
    case "pkcs7":
      return certPem;
    case "base64":
      return this.removePEMStructure(certPem);
    case "forge":
      return ownCertChain;
  }
  throw new Error("Unknown format identifier: " + format);
};

module.exports.getCertificateType = () => config.get().pki.ownCert.type;

module.exports.getPrivateKey = (format = "pem") => {
  switch (format.toLowerCase()) {
    case "pem":
      return privateKeyPem;
    case "base64":
      return this.removePEMStructure(privateKeyPem);
    case "forge":
      return forge.pki.privateKeyFromPem(privateKeyPem);
  }
  throw new Error("Unknown format identifier: " + format);
};

module.exports.getCACertificates = (format = "pem") =>
  caCertPems.map((certPem) => {
    switch (format.toLowerCase()) {
      case "pem":
        return certPem;
      case "base64":
        return this.removePEMStructure(certPem);
      case "forge":
        return forge.pki.privateKeyFromPem(certPem);
    }
    throw new Error("Unknown format identifier: " + format);
  });

function getPemTypeFromCertType(type) {
  switch (type.toLowerCase()) {
    case "x509-single":
    case "x509":
      return "CERTIFICATE";
    case "x509-pkcs7-chain":
    case "pkcs7":
      return "PKCS7";
    default:
      throw new Error("Unknkown certificate type: " + type);
  }
}

const forge = require("node-forge");

/*module.exports.createEd25519Signature = (msg, privateKey) =>
  sodium.crypto_sign_detached(msg, privateKey);*/

module.exports.createRSASignature = (msg, privateKey) => {
  // sign data using RSASSA-PSS where PSS uses a SHA-1 hash, a SHA-1 based
  // masking function MGF1, and a 20 byte salt
  var md = forge.md.sha1.create();
  md.update(msg, "utf8");
  var pss = forge.pss.create({
    md: forge.md.sha1.create(),
    mgf: forge.mgf.mgf1.create(forge.md.sha1.create()),
    saltLength: 20,
  });
  return privateKey.sign(md, pss);
};

module.exports.createSignature = (
  msg,
  privateKeyPem,
  alg = this.algs.rsa,
  format = "base64"
) => {
  const privateKey = forge.pki.privateKeyFromPem(privateKeyPem);
  var signature;
  switch (alg) {
    case this.algs.rsa:
      signature = this.createRSASignature(msg, privateKey);
      break;
    default:
      throw new Error("Unknown algorithm identifier");
  }
  switch (format) {
    case "base64":
      return forge.util.encode64(signature);
    case "raw":
      return signature;
    default:
      throw new Error("Unknown format string");
  }
};

module.exports.verifySignature = (
  msg,
  signature,
  publicKey,
  alg = this.algs.rsa,
  format = "base64"
) => {
  switch (format) {
    case "base64":
      signature = forge.util.decode64(signature);
      break;
    case "raw":
      break;
    default:
      console.log("Signature verification failed: Unknown format string");
      return false;
  }
  switch (alg) {
    case this.algs.rsa:
      return this.verifyRSASignature(msg, signature, publicKey);
    default:
      console.log(
        "Signature verification failed: Unknown algorithm identifier"
      );
      return false;
  }
};

module.exports.verifyRSASignature = (msg, signature, publicKey) => {
  // verify RSASSA-PSS signature
  var pss = forge.pss.create({
    md: forge.md.sha1.create(),
    mgf: forge.mgf.mgf1.create(forge.md.sha1.create()),
    saltLength: 20,
  });
  var md = forge.md.sha1.create();
  md.update(msg, "utf8");
  return publicKey.verify(md.digest().getBytes(), signature, pss);
};

module.exports.algs = {
  rsa: forge.pki.oids["RSASSA-PSS"],
};

module.exports.init = async () => true;

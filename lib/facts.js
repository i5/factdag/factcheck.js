const vl = require("./verboseLog");
const _moduleName = "facts.js";

let routes = [];

/**
 * Function used internally to retrieve facts.
 *
 *
 * @param {*} factID
 */
module.exports.getFactByID = async (factID) => {
  route = getFactRoute(factID);
  if (typeof route !== "function") {
    throw new Error("FactID could not be resolved to a route");
  }

  // Route may return an additional new factID (e.g. with added revision info)
  // In this case, the server module adds the more concrete id as an additional subfield.
  let result = await route(factID);
  let fact = result[0];

  if (!fact || fact === "") {
    throw {
      name: "FactNotFoundError",
      message: "The fact " + factID + " could not be retrieved",
    };
  }
  return result;
};

function getFactRoute(factID) {
  for (let i = 0; i < routes.length; i++) {
    if (routes[i].appliesTo(factID)) {
      return routes[i].get;
    }
  }
}

/**
 * API function to add a fact route.
 * A route is an object containing a priority (integer >=0), an appliesTo function,
 * and a get function, which both take a factID as their first argument.
 * Whenever a fact is to be retrieved, the library sorts the routes by ascending
 * priority, and uses the first function where the result of the appliesTo
 * function is true.
 * Then, the respective get function is used to retrieve the fact.
 * The result of the get function has to be an array of the form
 * [data, serialization, newFactID]
 * where data is the fact data, serialization is the string serialization identifier,
 * and the newFactID is a new (complete) fact ID supplied by the sender, if the receiver
 * left specified an incomplete ID (e.g., by ommitting the revision info to request the newest revision).
 * The newFactID can be ommitted.
 * Consider the examples for further information.
 *
 * @param {*} route The route to be added.
 */
module.exports.addRoute = (
  route = { priority: 1, appliesTo: (factID) => {}, get: (factID) => {} }
) => {
  if (!(typeof route.priority === "number" && route.priority >= 0)) {
    throw new Error("Priority was not a valid non-negative integer");
  }
  if (!(typeof route.appliesTo === "function")) {
    throw new Error("Supplied appliesTo handle is not a function");
  }
  if (!(typeof route.get === "function")) {
    throw new Error("Supplied get handle is not a function");
  }

  // Add the route at the correct position
  for (let i = routes.length - 1; i >= 0; i--) {
    if (routes[i].priority <= route.priority) {
      routes.splice(i + 1, 0, route);
      return;
    }
  }
  routes.splice(0, 0, route);

  vl(_moduleName, "New fact route added!");
};

module.exports.removeRoute = (routeToRemove) => {
  routes = routes.filter((route) => route !== routeToRemove);
  vl(_moduleName, "Fact route removed!");
};

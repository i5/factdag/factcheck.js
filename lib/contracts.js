const pki = require("./crypto/pki");
const facts = require("./facts");
const signatures = require("./crypto/signatures");
const checksums = require("./crypto/checksums");
const config = require("./config");
JSON.canonicalize = require("canonicalize");
const jsonld = require("jsonld");

// JSON-LD context based on the ReShare Ontology v0.2
// Documentation: https://i5.pages.rwth-aachen.de/factdag/reshare-ontology/0.2/
const _contractLDContext = {
  "@context": {
    fc: "http://i5.pages.rwth-aachen.de/factdag/reshare-ontology/0.2/#",
    xsd: "http://www.w3.org/2001/XMLSchema#",
    facts: {
      "@id": "fc:fact",
      "@context": {
        factID: { "@id": "fc:factOrigin", "@type": "@id" },
        //fullFactID: "fc:fullFactID",
        sha256: {
          "@id": "fc:sha256Sum",
          "@type": "xsd:hexBinary",
        },
        sha384: {
          "@id": "fc:sha384Sum",
          "@type": "xsd:hexBinary",
        },
        sha512: {
          "@id": "fc:sha512Sum",
          "@type": "xsd:hexBinary",
        },
        serialization: "fc:serialization",
      },
    },
    sender: {
      "@id": "fc:sender",
      "@context": {
        authID: "fc:identifies",
        type: "fc:certEncoding",
        //format: "fc:certFormat",
        cert: { "@id": "fc:x509Cert", "@type": "xsd:base64Binary" },
      },
    },
    receiver: {
      "@id": "fc:receiver",
      "@context": {
        authID: "fc:identifies",
        type: "fc:certEncoding",
        //format: "fc:certFormat",
        cert: { "@id": "fc:x509Cert", "@type": "xsd:base64Binary" },
      },
    },
    senderSig: {
      "@id": "fc:senderSig",
      "@context": {
        type: "fc:sigType",
        //format: "fc:sigFormat",
        sig: { "@id": "fc:sigData", "@type": "xsd:base64Binary" },
      },
    },
    receiverSig: {
      "@id": "fc:receiverSig",
      "@context": {
        type: "fc:sigType",
        //format: "fc:sigFormat",
        sig: { "@id": "fc:sigData", "@type": "xsd:base64Binary" },
      },
    },
    senderCustomContent: {
      "@id": "fc:senderCustomContent",
      "@type": "@json",
    },
    receiverCustomContent: {
      "@id": "fc:receiverCustomContent",
      "@type": "@json",
    },
    timestamp: {
      "@id": "fc:timestamp",
      "@type": "xsd:dateTime",
    },
  },
  "@type": "fc:DTC",
};

function contractLDContext() {
  return JSON.parse(JSON.stringify(_contractLDContext));
}

class Contract {
  constructor() {
    this._content = {};
  }

  static fromMessage(message) {
    const contract = new Contract();
    contract._content = message.contract;
    if (message.contractIRI) {
      contract.contractIRI = message.contractIRI;
    }
    return contract;
  }

  static fromObject(obj) {
    const contract = new Contract();
    contract._content = obj;
    return contract;
  }

  static fromJSON(str) {
    const contract = new Contract();
    contract._content = JSON.parse(str);
    return contract;
  }

  static async fromJSONLD(doc) {
    //WIP. Parsing does not work and is not complete.
    if (typeof doc === "string") {
      doc = JSON.parse(doc);
    }

    const contract = new Contract();
    let content = {};

    const flattened = await jsonld.flatten(doc);

    // Get contract IRI
    let contractIRI;
    const contractClassIRI = contractLDContext()["@context"].fc + "DTC";
    const contracts = flattened.filter(
      (object) => object["@type"] && object["@type"].includes(contractClassIRI)
    );
    if (contracts.length == 0) {
      throw new Error("No contract found in the supplied JSON-LD document!");
    } else if (contracts.length > 1) {
      throw new Error(
        "More than one contract was found in the supplied JSON-LD document!"
      );
    }
    // Exactly one contract in doc
    contractIRI = contracts[0]["@id"];

    //----- Parse facts -----
    //Initialize
    content.facts = [];
    // Create special-purpose context
    const factContext = contractLDContext()["@context"].facts["@context"];
    factContext.fc = contractLDContext()["@context"].fc;
    factContext.xsd = contractLDContext()["@context"].xsd;
    // Filter the flattened document for facts by their ontology type
    const factClassIRI = contractLDContext()["@context"].fc + "Fact";
    const ldFacts = flattened.filter(
      (object) => object["@type"] && object["@type"].includes(factClassIRI)
    );
    // Compactify with the special-purpose context
    const compactFacts = await Promise.all(
      ldFacts.map((ldFact) => jsonld.compact(ldFact, factContext))
    );
    // Map values to a new fact object
    compactFacts.forEach((cFact) => {
      let fact = {};
      Object.entries(cFact).forEach(([key, value]) => {
        if (key.charAt(0) === "@") {
          return;
        }
        if (typeof value === "object" && value["@value"]) {
          fact[key] = value["@value"];
        } else {
          fact[key] = value;
        }
      });
      content.facts.push(fact);
    });

    //----- Unified async function for parsing field contents for identities and signatures -----
    async function parseField(field) {
      // Create special-purpose context
      const context = contractLDContext()["@context"][field]["@context"];
      context.fc = contractLDContext()["@context"].fc;
      context.xsd = contractLDContext()["@context"].xsd;
      // Filter flattened document for the field by its default IRI
      const ldContent = flattened.filter(
        (object) => object["@id"] && object["@id"] === contractIRI + "#" + field
      )[0];
      if (ldContent === undefined) {
        return;
      }
      // Compactify
      const cField = await jsonld.compact(ldContent, context);
      // create field object
      const fieldContent = {};
      Object.entries(cField).forEach(([key, value]) => {
        if (key.charAt(0) === "@") {
          return;
        }
        if (typeof value === "object" && value["@value"]) {
          fieldContent[key] = value["@value"];
        } else {
          fieldContent[key] = value;
        }
      });
      content[field] = fieldContent;
    }
    await Promise.all(
      ["sender", "receiver", "senderSig", "receiverSig"].map((field) =>
        parseField(field)
      )
    );

    // Working solution (not optimal): Currently only base64 signatures certificates are used.
    // The type is therefore fixed here. Reading the variable type would require additional effort, but is possible.
    content.receiver.format = "base64";
    content.sender.format = "base64";
    content.receiverSig.format = "base64";
    content.senderSig.format = "base64";

    //----- Parse timestamp -----
    const ldTimestamps =
      contracts[0][contractLDContext()["@context"].fc + "timestamp"];
    if (ldTimestamps) {
      const ldTimestamp = ldTimestamps[0];
      content.timestamp =
        typeof ldTimestamp === "object" && ldTimestamp["@value"]
          ? ldTimestamp["@value"]
          : ldTimestamp;
    }

    //----- Parse custom content -----
    const ldReceiverCCs =
      contracts[0][
        contractLDContext()["@context"].fc + "receiverCustomContent"
      ];
    if (ldReceiverCCs) {
      const ldReceiverCC = ldReceiverCCs[0];
      content.receiverCustomContent =
        typeof ldReceiverCC === "object" && ldReceiverCC["@value"]
          ? ldReceiverCC["@value"]
          : ldReceiverCC;
    }
    const ldSenderCCs =
      contracts[0][contractLDContext()["@context"].fc + "senderCustomContent"];
    if (ldSenderCCs) {
      const ldSenderCC = ldSenderCCs[0];
      content.senderCustomContent =
        typeof ldSenderCC === "object" && ldSenderCC["@value"]
          ? ldSenderCC["@value"]
          : ldSenderCC;
    }

    contract._content = content;
    contract.contractIRI = contractIRI;
    return contract;
  }

  updateWithMessage(message) {
    // Used to reuse existing partial contracts in stateful mode
    let newContent = message.contract;
    Object.assign(this._content, newContent);
  }

  sortFacts() {
    // Sort facts lexicographically
    this._content.facts.sort((a, b) => {
      if (a.factID < b.factID) {
        return -1;
      } else if (a.factID > b.factID) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  addFactIDs(factIDs) {
    if (!factIDs instanceof Array) {
      throw new TypeError("FactIDs must be supplied as an array of strings!");
    }
    if (!this._content.facts) {
      this._content.facts = [];
    }
    factIDs.forEach((factID) => {
      this._content.facts.push({ factID });
    });
    return this;
  }

  addReceiverCustomContent(customContent) {
    // Check if customContent has any values (which can be stringified)
    if (
      typeof customContent === "object" &&
      JSON.stringify(customContent) !== "{}"
    ) {
      this._content.receiverCustomContent = customContent;
    }
  }

  addSenderCustomContent(customContent) {
    // Check if customContent has any values (which can be stringified)
    if (
      typeof customContent === "object" &&
      JSON.stringify(customContent) !== "{}"
    ) {
      this._content.senderCustomContent = customContent;
    }
  }

  addSender(authID) {
    this._content.sender = {
      type: pki.getCertificateType(),
      format: "base64",
      cert: pki.getCertificateChain("base64"),
    };
    if (authID) {
      this._content.sender.authID = authID;
    }
    return this;
  }

  addReceiver(authID) {
    this._content.receiver = {
      type: pki.getCertificateType(),
      format: "base64",
      cert: pki.getCertificateChain("base64"),
    };
    if (authID) {
      this._content.receiver.authID = authID;
    }
    return this;
  }

  // async, as it uses the getFactByID function
  async addChecksums() {
    let factsFound = await Promise.all(
      this._content.facts.map((fact) => this.addChecksum(fact.factID))
    );

    let foundOneFact = factsFound.some((factFound) => factFound);
    if (!foundOneFact) {
      throw new Error("None of the FactIDs could be resolved!");
    }
    return this;
  }

  async addChecksum(factID) {
    try {
      let [
        factSerialization,
        serializationMethod,
        newID,
      ] = await facts.getFactByID(factID);

      const factsWithSameID = this._content.facts.filter(
        (fact) => fact.factID === factID
      );
      if (factsWithSameID.length !== 1) {
        return false;
      }
      let fact = factsWithSameID[0];

      if (newID) {
        fact.requestedID = fact.factID;
        fact.factID = newID;
      }

      fact.sha256 = checksums.sha256sum(factSerialization);
      fact.serialization = serializationMethod;
      return true;
    } catch (e) {
      if (e.name === "FactNotFoundError") {
        console.log(e.message);
        return false;
      } else {
        throw e;
      }
    }
  }

  addSenderSig() {
    this.sortFacts();
    const contentToSign = this.contentToSign();
    //Create RSA-PSS signature
    const signature = {
      type: signatures.algs.rsa, // according to RFC 4056
      format: "base64",
      sig: signatures.createSignature(
        JSON.canonicalize(contentToSign),
        pki.getPrivateKey("pem"),
        signatures.algs.rsa
      ),
    };
    this._content.senderSig = signature;
    return this;
  }

  addReceiverSig() {
    const contentToSign = this.contentToSign();
    //Create RSA-PSS signature
    const signature = {
      type: signatures.algs.rsa, // according to RFC 4056
      format: "base64",
      sig: signatures.createSignature(
        JSON.canonicalize(contentToSign),
        pki.getPrivateKey("pem"),
        signatures.algs.rsa
      ),
    };
    this._content.receiverSig = signature;
    return this;
  }

  addTimestamp(format = "iso") {
    switch (format.toLowerCase()) {
      case "iso":
        this._content.timestamp = new Date().toISOString();
        break;
      default:
        throw new Error(`Unrecognized timestamp format: ${format}`);
    }
  }

  hasTimestamp = () =>
    this._content.timestamp && typeof this._content.timestamp === "string";

  timestampIsRecent() {
    if (!this.hasTimestamp()) {
      return false;
    }
    let { timestamp } = this._content;
    try {
      let date = new Date(timestamp);
      let age = new Date() - date;
      let maxAgeMs = config.get().maximumTimestampAge * 1000 || 60 * 1000;
      if (age >= 0 && age <= maxAgeMs) {
        return true;
      }
    } catch (err) {
      console.log("Timestamp validation failed: " + err.message);
    }
    return false;
  }

  contentToSign() {
    const copiedContent = this.getContent("copy");
    delete copiedContent.senderSig;
    delete copiedContent.receiverSig;
    return copiedContent;
  }

  async verifySenderSig() {
    try {
      const { senderSig, sender } = this._content;
      const signedContent = this.contentToSign();

      return signatures.verifySignature(
        JSON.canonicalize(signedContent),
        senderSig.sig,
        pki.getPublicKeyFromCertificate(
          sender.cert,
          sender.type,
          sender.format
        ),
        senderSig.type,
        senderSig.format
      );
    } catch (err) {
      console.log("SenderSig verification failed because of error:");
      console.log(err);
      return false;
    }
  }

  async verifyReceiverSig() {
    try {
      const { receiverSig, receiver } = this._content;
      const signedContent = this.contentToSign();

      return signatures.verifySignature(
        JSON.canonicalize(signedContent),
        receiverSig.sig,
        pki.getPublicKeyFromCertificate(
          receiver.cert,
          receiver.type,
          receiver.format
        ),
        receiverSig.type,
        receiverSig.format
      );
    } catch (err) {
      console.log("ReceiverSig verification failed because of error:");
      console.log(err);
      return false;
    }
  }

  async verifySenderCertificate() {
    try {
      const { sender } = this._content;
      return pki.verifyCertificate(sender.cert, sender.type, sender.format);
    } catch (err) {
      console.log("Sender certificate verification failed because of error:");
      console.log(err);
      return false;
    }
  }

  async verifyReceiverCertificate() {
    try {
      const { receiver } = this._content;
      return pki.verifyCertificate(
        receiver.cert,
        receiver.type,
        receiver.format
      );
    } catch (err) {
      console.log("Receiver certificate verification failed because of error:");
      console.log(err);
      return false;
    }
  }

  async isUnmodified() {
    // Only used by the sender upon receiving R2.
    // Is used to check that the receiver has not modified the contract,
    // by verifying the senderSignature with the own key (for which the receiver cannot forge a signature).
    const { senderSig } = this._content;
    const myCertChain = pki.getCertificateChain("forge");
    const signedContent = this.contentToSign();
    return signatures.verifySignature(
      JSON.canonicalize(signedContent),
      senderSig.sig,
      myCertChain[0].publicKey,
      senderSig.type,
      senderSig.format
    );
  }

  verifyChecksums(factData) {
    let res = { result: true, errors: [] };
    Object.entries(factData).forEach(([factID, data]) => {
      let factsWithSameID = this._content.facts.filter(
        (fact) => fact.factID === factID
      );
      if (factsWithSameID.length == 1) {
        const fact = factsWithSameID[0];
        const checksumsInContract = checksums.supportedAlgs
          .map((alg) => [alg, fact[alg]])
          .filter(([alg, checksum]) => checksum);
        if (checksumsInContract.length === 0) {
          res.result = false;
          res.errors.push(`No checksums found for ${factID}`);
        }
        checksumsInContract.forEach(([alg, value]) => {
          if (!checksums.verify(alg, data, value)) {
            res.result = false;
            res.errors.push(`Checksum for ${factID} (${alg}) did not verify`);
          }
        });
      } else {
        res.result = false;
        res.errors.push(
          "Multiple facts with the same FactID in conract (FactID: " +
            factID +
            ")"
        );
      }
    });
    return res;
  }

  async getLDContent(format = "jsonld") {
    this.sortFacts();
    let content = JSON.parse(JSON.canonicalize(this.getContent("copy")));

    // Load static LD context
    let context = Object.assign({}, contractLDContext()["@context"]);
    content["@context"] = context;
    content["@type"] = contractLDContext()["@type"];

    //temporary!
    this.contractIRI = "http://example.com/contracts/1";

    // Set contract IRI
    if (
      this.contractIRI &&
      typeof this.contractIRI === "string" &&
      this.contractIRI.length > 0
    ) {
      content["@id"] = this.contractIRI;
      context.contract = this.contractIRI + "#";
    } else {
      throw new Error(
        "Could not be exported as JSON-LD: No contract IRI defined!"
      );
    }

    // Assign IRIs and types to fact entries (contract:fact-0,...)
    // This is why we canonicalize the content beforehand, as we now have a canonical
    // order of facts.
    let factIDs = content.facts.map((fact) => fact.factID).sort();
    content.facts.forEach((fact) => {
      fact["@id"] = "contract:fact-" + factIDs.indexOf(fact.factID);
      fact["@type"] = "fc:Fact";
    });

    // Assign sender, receiver, senderSig and receiverSig IRIs
    content.sender["@id"] = "contract:sender";
    content.receiver["@id"] = "contract:receiver";
    content.senderSig["@id"] = "contract:senderSig";
    content.receiverSig["@id"] = "contract:receiverSig";

    // Add type relationships to contract contents
    content.sender["@type"] = "fc:Sender";
    content.receiver["@type"] = "fc:Receiver";
    content.senderSig["@type"] = "fc:Signature";
    content.receiverSig["@type"] = "fc:Signature";

    // expand and compact to beautify
    const compacted = await jsonld.compact(
      await jsonld.expand(content),
      context
    );

    switch (format.toLowerCase()) {
      case "jsonld":
        return JSON.stringify(compacted);
      case "jsonld-internal":
      case "internal":
        return compacted;
      case "jsonld-expanded":
        return JSON.stringify(await jsonld.expand(compacted));
      case "jsonld-flattened":
        return JSON.stringify(await jsonld.flatten(compacted));
      case "normalized-nquads":
      case "normalized":
        return await jsonld.normalize(compacted);
      case "nquads":
        return await jsonld.toRDF(compacted);
      default:
        throw new Error("Unsupported LD contract format: " + format);
    }
  }

  getContent(format = "json") {
    this.sortFacts();
    switch (format.toLocaleLowerCase().replace(/[-_]/, "")) {
      case "internal":
        return this._content;
      case "copy":
        return JSON.parse(JSON.stringify(this._content));
      case "json":
        return JSON.canonicalize(this._content);
      case "jsonld":
        throw new Error(
          "The jsonld format is not supported by getContent. Please use getLDContent instead."
        );
      default:
        throw new Error(
          "Unrecognized contract serialization format: " + format
        );
    }
  }
}

// Helper function to remove JSON LD keys from objects
function removeJSONLD(obj) {
  if (typeof obj === "object") {
    delete obj["@id"];
    delete obj["@context"];
    delete obj["@type"];
    Object.entries(obj).forEach(([key, value]) => {
      removeJSONLD(value);
    });
  }
}

module.exports = {
  Contract,
};

const net = require("net");
const tls = require("tls");
const pki = require("./crypto/pki");
const { makeErrorMessage } = require("./messages");
const config = require("./config");
const vl = require("./verboseLog");
const { md5sum } = require("./crypto/checksums");

const _moduleName = "conn.js";

class ServerCache {
  // Server cache implementation for fragmented messages.
  // Lifetime can be set in the config (value in ms).
  // Cache content is sorted by the remotePort (one array per port).
  // The sequence of entries of the array should correspond to the time of arrival.
  // The cache entries have a hash generated from the data to be deleted selectively after a timeout.
  constructor(lifetime = 2000, onTimeout = (data, remotePort) => {}) {
    this.content = {};
    this.lifetime = lifetime;
    this.onTimeout = onTimeout;
  }

  add(data, remotePort) {
    let hash = md5sum(data);
    this.content[remotePort] = this.content[remotePort] || [];
    this.content[remotePort].push({ hash, data });
    setTimeout(() => {
      if (this.remove(hash, remotePort)) {
        onTimeout(data, remotePort);
      }
    }, this.lifetime);
    return hash;
  }

  clear(remotePort) {
    delete this.content[remotePort];
  }

  remove(hash, remotePort) {
    // TODO: write error message to client if cache was not empty
    let found = false;
    if (this.content[remotePort] && this.content[remotePort].length > 0) {
      this.content[remotePort] = this.content[remotePort].filter(
        (value, index, arr) => {
          if (value.hash !== hash) {
            found = true;
            return true;
          }
        }
      );
      if (this.content[remotePort].length === 0) {
        delete this.content[remotePort];
      }
    }
    return found;
  }

  get(remotePort) {
    let content = this.content[remotePort];
    return content ? content.map((entry) => entry.data) : [];
  }
}

module.exports.ServerCache = ServerCache;

class ClientCache {
  // Client cache implementation for fragmented messages.
  // Lifetime can be set in the config (value in ms).
  // The sequence of entries of the array should correspond to the time of arrival.
  // The cache entries have a hash generated from the data to be deleted selectively after a timeout.
  constructor(lifetime = 2000, onTimeout = (data) => {}) {
    this.content = [];
    this.lifetime = lifetime;
    this.onTimeout = onTimeout;
  }
  add(data) {
    let hash = md5sum(data);
    this.content.push({ hash, data });
    // TODO: write error message to server if cache was not empty
    setTimeout(() => {
      if (this.remove(hash)) {
        this.onTimeout(data);
      }
    }, this.lifetime);
    return hash;
  }

  clear() {
    this.content = [];
  }

  remove(hash) {
    let found = false;
    this.content = this.content.filter((value, index, arr) => {
      if (value.hash !== hash) {
        found = true;
        return true;
      }
    });
    return found;
  }

  get() {
    let { content } = this;
    return content ? content.map((entry) => entry.data) : [];
  }
}

module.exports.ClientCache = ClientCache;

module.exports.createServer = (
  port,
  useTls = config.get().useTls,
  onConnection = (connection) => {}
) => {
  let cache = new ServerCache(config.get().cacheLifetime || 2000);

  if (useTls) {
    vl(_moduleName, "Creating TLS server");
    let options = {
      key: pki.getPrivateKey("pem"),
      cert: pki.getCertificateChain("pem"),
      ca: pki.getCACertificates("pem"),
    };
    return tls.createServer(options, handleConnection).listen(port);
  } else {
    vl(_moduleName, "Creating TCP server");
    let server = net.createServer(handleConnection);
    server.listen(port);
    return server;
  }

  function handleConnection(socket) {
    vl(_moduleName, "Server: Incoming connection");
    const connection = {
      close: () => {
        socket.end();
      },
      onMessage: (msg) => {},
      onError: (err) => {},
      writeMessage: writeMessage,
    };
    socket.on("data", (data) => handleData(data, socket.remotePort));
    socket.on("error", (err) => connection.onError(err));
    socket.on("close", (hadErrors) => {
      vl(_moduleName, "Server: Connection closed");
    });
    onConnection(connection);

    function handleData(data, remotePort) {
      vl(_moduleName, "Server: Incoming data");
      try {
        //Try to parse the data as UTF-8 encoded JSON
        var msg = data.toString("utf8");
        var parsedMsg = JSON.parse(msg.trim());
        connection.onMessage(parsedMsg);
      } catch (err) {
        if (err instanceof SyntaxError) {
          // Invalid JSON
          if (config.get().enableCaching) {
            // Extra check: If cache is empty and message does not start with a "{", reject
            if (msg.charAt(0) !== "{" && cache.get(remotePort).length === 0) {
              socket.write(
                JSON.stringify(
                  makeErrorMessage("Message was not valid JSON!")
                ) + "\n"
              );
            } else {
              // Assume fragmented message. (TODO: When to throw the error?)
              // 1: Try to assemble messages in the cache
              let cacheData = cache.get(remotePort);
              cacheData.push(data);
              let assembledMsg = Buffer.concat(cacheData).toString("utf8");
              try {
                let parsedMsg = JSON.parse(assembledMsg.trim());
                cache.clear(remotePort);
                connection.onMessage(parsedMsg);
              } catch (err2) {
                if (err2 instanceof SyntaxError) {
                  // Assembled message still is not valid JSON. Put data into cache.
                  cache.add(data, remotePort);
                } else {
                  throw err2;
                }
              }
            }
          } else {
            // Cache is disabled. Message was not valid JSON. (May be caused by message fragmentation)
            connection.writeMessage(
              makeErrorMessage("Message was not valid JSON!")
            );
          }
        } else {
          throw err;
        }
      }
    }
    function writeMessage(msg) {
      socket.write(JSON.stringify(msg) + "\n");
    }
  }
};

module.exports.createClientConnection = (
  port,
  host = "localhost",
  onConnection = () => {},
  onMessage = (parsedMsg) => {}, // called when a valid JSON message is received
  onError = (err) => {},
  evalPortCallback = (port) => {} // Remove in production
) => {
  var socket;
  let tryingTLS = true;

  let cache = new ClientCache(config.get().cacheLifetime || 2000);

  let _c = config.get();
  if (_c.useTls === false) {
    // Disable auto-detection and directly try TCP
    vl(_moduleName, "Client: Making TCP connection (without auto-detection)");
    tryTCP();
  } else {
    tryTLS();
  }

  return {
    close: () => {
      socket.end();
    },
    writeMessage: writeMessage,
  };

  function tryTLS() {
    // Try TLS
    vl(_moduleName, "Client: Trying to connect via TLS...");
    let options = {
      key: pki.getPrivateKey("pem"),
      cert: pki.getCertificateChain("pem"),
      ca: pki.getCACertificates("pem"),
    };
    try {
      socket = tls.connect(port, options, function () {
        tryingTLS = false;
        if (socket.authorized) {
          vl(_moduleName, "Client: TLS Connection established!");
          onConnection();
        } else {
          vl(_moduleName, "Client: TLS didnt work! Switching to TCP...");
          tryTCP();
        }
      });

      // TLS eval port callback on connect (not secureConnect!)
      socket.on("connect", () => {
        evalPortCallback(socket.localPort);
      });
    } catch (err) {
      tryingTLS = false;
      vl(_moduleName, "Client: TLS error: " + err.message);
      vl(_moduleName, "Client: TLS didnt work! Switching to TCP...");
      tryTCP();
    }
    setListeners();
  }

  function tryTCP() {
    if (config.get().clientEnforceTls) {
      onError(TCP_NOT_ALLOWED_MESSAGE);
      return;
    }

    socket = net.createConnection(port, host, () => {
      vl(_moduleName, "Client: TCP Connection established!");
      onConnection();
    });

    // TCP eval port callback
    socket.on("connect", () => {
      evalPortCallback(socket.localPort);
    });

    setListeners();
  }

  function setListeners() {
    socket.on("data", handleData);
    socket.on("error", (err) => {
      if (tryingTLS && err.code === "ERR_SSL_WRONG_VERSION_NUMBER") {
        tryingTLS = false;
        vl(_moduleName, "Client: TLS didnt work! Switching to TCP...");
        tryTCP();
      } else {
        onError(err.message);
      }
    });
    socket.on("end", (err) => {
      if (err) {
        onError(
          "Communication partner ended the communication (" + err.message + ")"
        );
      }
    });
    socket.on("close", (hadErrors) => {
      vl(_moduleName, "Connection closed");
      if (hadErrors && !tryingTLS) {
        onError("Socket was closed unexpectedly");
      }
    });
  }

  // Data handler, called when data is received on the socket (see setListeners -> socket.on("data",...))
  function handleData(data) {
    vl(_moduleName, "Client: Incoming data");
    try {
      //Try to parse the data as UTF-8 encoded JSON
      var msg = data.toString("utf8");
      var parsedMsg = JSON.parse(msg.trim());
      onMessage(parsedMsg);
    } catch (err) {
      if (err instanceof SyntaxError) {
        // Invalid JSON
        if (config.get().enableCaching) {
          //Cache is enabled
          // Extra check: If cache is empty and message does not start with a "{", reject
          if (msg.charAt(0) !== "{" && cache.get().length === 0) {
            socket.write(
              JSON.stringify(makeErrorMessage("Message was not valid JSON!")) +
                "\n"
            );
          } else {
            // Assume fragmented message. (TODO: When to throw the error?)
            // 1: Try to assemble messages in the cache
            let cacheData = cache.get();
            cacheData.push(data);
            let assembledMsg = Buffer.concat(cacheData).toString("utf8");
            try {
              let parsedMsg = JSON.parse(assembledMsg.trim());
              cache.clear();
              onMessage(parsedMsg);
            } catch (err2) {
              if (err2 instanceof SyntaxError) {
                // Assembled message still is not valid JSON. Put data into cache.
                cache.add(data);
              } else {
                throw err2;
              }
            }
          }
        } else {
          // Cache is disabled. Message was not valid JSON. (May be caused by message fragmentation)
          writeMessage(makeErrorMessage("Message was not valid JSON!"));
        }
      } else {
        throw err;
      }
    }
  }

  function writeMessage(msg) {
    vl(_moduleName, "Client: Outgoing data");
    socket.write(JSON.stringify(msg) + "\n");
  }
};

module.exports.TCP_NOT_ALLOWED_MESSAGE =
  "Fallback to TCP not allowed! Connection could not be established!";
const { TCP_NOT_ALLOWED_MESSAGE } = this;

const { Contract } = require("./contracts");
const config = require("./config");

module.exports.makeR1 = (
  factIDs,
  customContent,
  authID,
  supportStatefulMode
) => {
  const contract = new Contract();
  contract.addFactIDs(factIDs);
  contract.addReceiverCustomContent(customContent);
  contract.addReceiver(authID);
  const message = {
    messageType: "ContractRequest",
    contract: contract.getContent("copy"),
  };
  if (supportStatefulMode) {
    message.supportStatefulMode = true;
  }
  return [message, contract];
};

module.exports.makeR2 = (contract, stateKey) => {
  contract.addReceiverSig();

  const contractMessage = {
    messageType: "ReceiverContract",
  };
  if (stateKey) {
    //Stateful mode to reduce message sizes
    contractMessage.stateKey = stateKey;
    contractMessage.contract = {
      receiverSig: contract.getContent("copy").receiverSig,
    };
  } else {
    contractMessage.contract = contract.getContent("copy");
  }
  if (contract.contractIRI) {
    contractMessage.contractIRI = contract.contractIRI;
  }
  return contractMessage;
};

//async, because it uses addChecksums
module.exports.makeS1 = async (
  contract,
  customContent,
  contractIRICallback,
  authID,
  stateKey
) => {
  contract.addSender(authID);
  contract.addSenderCustomContent(customContent);
  contract.addTimestamp();
  const contractIRI = contractIRICallback(contract);
  if (contractIRI) {
    contract.contractIRI = contractIRI;
  }

  try {
    await contract.addChecksums();
  } catch (err) {
    console.log(err);
    return this.makeErrorMessage("None of the facts could be retrieved!");
  }
  contract.addSenderSig();

  const contractMessage = {
    messageType: "SenderContract",
    contract: contract.getContent("copy"),
  };
  if (stateKey) {
    //Stateful mode to reduce message sizes
    contractMessage.stateKey = stateKey;
    delete contractMessage.contract.receiver;
    delete contractMessage.contract.receiverCustomContent;
  }
  if (contract.contractIRI) {
    contractMessage.contractIRI = contract.contractIRI;
  }

  return contractMessage;
};

module.exports.makeErrorMessage = (msg) => {
  return {
    messageType: "Error",
    errorMessage: msg,
  };
};

const conn = require("./conn");
const { makeErrorMessage, makeS1 } = require("./messages");
const schemas = require("./schemas");
const { Contract } = require("./contracts");
const vl = require("./verboseLog");
const { contract } = require("./schemas");
const { random, util } = require("node-forge");

const _moduleName = "client.js";

/**
 * API function to create a TCP/TLS server.
 * Listens on a given port.
 *
 * @param {*} port The port on which the server module listens
 * @param {*} options Options object (see below)
 */
module.exports.create = (
  port,
  options = {
    useTls: true, // Specify whether TLS is enabled. True is recommended in any non-local deployment.
    onContract: (c) => {}, // Callback for successful DTC generation
    onError: (e) => {}, // Error callback (called with string)
    customContentCallback: function (contract) {}, // Callback to add the custom content.
    // Implemented as a callback to generate the custom content depending on the R1 message.
    contractIRICallback: function (contract) {}, // Callback to add the contract IRI for identification as LD.
    // Implemented as a callback to generate the contract IRI depending on the R1 message.
    authID, // authority ID of the sender
    supportStatefulMode: false, // Specify whether to support the stateful mode. Does not work with non-persistent load balancing.
    stateLifetimeMs: 5000, // Lifetime of the state cache used in stateful mode.
  }
) => {
  let {
    useTls,
    onContract,
    onError,
    customContentCallback,
    contractIRICallback,
    authID,
    supportStatefulMode,
    stateLifetimeMs,
  } = options;
  useTls = useTls !== false;
  onContract = onContract || ((contract) => {});
  onError = onError || ((err) => {});
  customContentCallback =
    typeof customContentCallback === "function"
      ? customContentCallback
      : function (contract) {};
  contractIRICallback =
    typeof contractIRICallback === "function"
      ? contractIRICallback
      : function (contract) {};
  authID = typeof authID === "string" ? authID : undefined;
  supportStatefulMode = supportStatefulMode ? true : false;

  const serverInstance = {};

  //----- Type checks -----
  if (typeof port !== "number" || port <= 0 || port > 65535) {
    throw new TypeError(
      "Invalid argument: Port was either not a number or not in the correct range"
    );
  }

  if (typeof useTls !== "boolean") {
    throw new TypeError("Invalid argument: useTls is not boolean");
  }

  if (typeof onContract !== "function") {
    throw new TypeError("Invalid argument: onContract is not a function");
  }

  if (typeof onError !== "function") {
    throw new TypeError("Invalid argument: onError is not a function");
  }

  //----- Setting changeable variables -----
  serverInstance.onContract = onContract;
  serverInstance.onError = onError;

  //----- Initialize state cache -----
  let state = supportStatefulMode ? new StateCache() : undefined;

  //----- Create server using conn.js -----
  let server = conn.createServer(port, useTls, handleConnection);

  serverInstance.close = () => {
    server.close();
    console.log("Server closed!");
  };

  function handleConnection(connection) {
    vl(_moduleName, "Server: New ingoing connection");
    let context = {
      writeMessage: connection.writeMessage,
      close: connection.close,
      authID,
      onError: serverInstance.onError,
      onContract: serverInstance.onContract,
      customContentCallback,
      contractIRICallback,
      supportStatefulMode,
      state,
      stateLifetimeMs,
    };
    connection.onMessage = (msg) => handleMessage(msg, context);
    connection.onError = onError;
  }
  // Return server instance here
  return serverInstance;
};

/**
 * Express middleware to integrate factcheck.js into an existing Webserver.
 * Wraps the factcheck communication into two HTTP POST calls.
 *
 * @param {*} options Options object.
 */
module.exports.expressMiddleware = (
  options = {
    onContract: (contract) => {}, // Callback for successful DTC generation
    onError: (error) => {}, // Error callback (called with string)
    customContentCallback: function (contract) {}, // Callback to add the custom content.
    // Implemented as a callback to generate the custom content depending on the R1 message.
    contractIRICallback: function (contract) {}, // Callback to add the contract IRI for identification as LD.
    // Implemented as a callback to generate the contract IRI depending on the R1 message.
    authID, // authority ID of the sender
    supportStatefulMode: false, // Specify whether to support the stateful mode. Does not work with non-persistent load balancing.
    stateLifetimeMs, // Lifetime of the state cache used in stateful mode.
  }
) => {
  let {
    onContract,
    onError,
    customContentCallback,
    contractIRICallback,
    authID,
    supportStatefulMode,
    stateLifetimeMs,
  } = options;
  onContract = onContract || ((contract) => {});
  onError = onError || ((err) => {});
  customContentCallback =
    typeof customContentCallback === "function"
      ? customContentCallback
      : function (contract) {};
  contractIRICallback =
    typeof contractIRICallback === "function"
      ? contractIRICallback
      : function (contract) {};
  supportStatefulMode = supportStatefulMode ? true : false;
  authID = typeof authID === "string" ? authID : undefined;

  //----- Initialize state cache -----
  let state = supportStatefulMode ? new StateCache() : undefined;

  return (req, res, next) => {
    function writeMessage(msg) {
      res.json(msg);
    }

    let context = {
      writeMessage,
      close: () => {
        res.status(200).end();
      },
      authID,
      onError,
      onContract,
      customContentCallback,
      contractIRICallback,
      supportStatefulMode,
      state,
      stateLifetimeMs,
    };

    try {
      res.setHeader("Content-Type", "application/json");
      const parsedMsg = req.body;
      handleMessage(parsedMsg, context);
    } catch (err) {
      if (err instanceof SyntaxError) {
        context.writeMessage(makeErrorMessage("Message was not valid JSON!"));
      } else {
        throw err;
      }
    }
  };
};

// Connection-dependent functions
// Refactored out of the create->handleConnection block
// to also be used in the express middleware, thus passing context
// function is necessary as they only exist within the scope
// of a connection/request-response pair

function handleMessage(parsedMsg, context) {
  vl(_moduleName, "Server: New message");
  // Determine message type
  // Possible types (for server):
  //   R1: Initial Request
  //   R2: Contract including ReceiverSig
  // R2 is checked first, as valid R2 messages are a subset
  // of valid R1 messages
  if (
    schemas.r2.validate(parsedMsg) &&
    (schemas.contract.r2.validate(parsedMsg.contract) ||
      (parsedMsg.stateKey &&
        schemas.contract.r2Stateful.validate(parsedMsg.contract)))
  ) {
    //Message is a valid R2 message
    vl(_moduleName, "Server: Received R2 message");
    handleR2(parsedMsg, context);
  } else if (
    schemas.r1.validate(parsedMsg) &&
    schemas.contract.r1.validate(parsedMsg.contract)
  ) {
    vl(_moduleName, "Server: Received R1 message");
    //Message is a valid R1 message
    handleR1(parsedMsg, context);
  } else if (schemas.error.validate(parsedMsg)) {
    vl(_moduleName, "Server: Received error message");
    context.onError("Received error from client: " + parsedMsg.errorMessage);
  } else {
    context.writeMessage(makeErrorMessage("Unrecognized message format!"));
  }
}

function handleR1(parsedMsg, context) {
  const contract = Contract.fromMessage(parsedMsg);
  // On a R1 message, you don't care about the validity of the receiver's
  // certificate, unless you use it for authorization etc.
  // Therefore, no checks are done here. The certificate is validated
  // together with receiverSig upon handling R2.
  // The only thing which needs to be checked is that the timestamp (if present)
  // is recent enough, as per default value or specified in the config.
  context.customContent = context.customContentCallback(contract);

  // Stateful mode is used iff the server is configured
  // to support it and the client declared support in R1
  if (context.supportStatefulMode && parsedMsg.supportStatefulMode) {
    const stateKey = context.state.add(contract, context.stateLifetimeMs);
    context.contractStateKey = stateKey;
  }
  sendS1(contract, context);
}

function handleR2(parsedMsg, context) {
  try {
    let contract, contractStateKey;
    if (parsedMsg.stateKey) {
      if (!context.supportStatefulMode) {
        throw new MessageError(
          "Received message indicated stateful mode while " +
            "stateful mode is not supported client-side"
        );
      }
      const { state } = context;
      contractStateKey = parsedMsg.stateKey;
      if (!state.has(contractStateKey)) {
        throw new MessageError(
          "The supplied state key is either invalid or expired!"
        );
      }
      contract = state.get(contractStateKey);
      contract.updateWithMessage(parsedMsg);
    } else {
      contract = Contract.fromMessage(parsedMsg);
    }

    // Checks
    Promise.all([
      contract.verifyReceiverCertificate(), // Verify the receiver certificate
      contract.verifyReceiverSig(), // Verify the receiverSig
      contract.isUnmodified(), // Verify that the receiver has not modified the contract
    ]).then((results) => {
      if (results.every((result) => result)) {
        vl(_moduleName, "All server-side checks passed, accepting contract");
        // State can be removed now
        if (contractStateKey) {
          context.state.remove(contractStateKey);
        }
        context.onContract(contract);
        context.close();
      } else {
        throw new MessageError(
          "Could not verify receiver certficate or receiver cert!"
        );
      }
    });
  } catch (err) {
    // Only catch custom MessageError instances here
    if (err instanceof MessageError) {
      context.onError(err);
      context.writeMessage(makeErrorMessage(err.message));
    } else {
      throw err;
    }
  }
}

async function sendS1(contract, context) {
  vl(_moduleName, "Server: Sending S1");
  makeS1(
    contract,
    context.customContent,
    context.contractIRICallback,
    context.authID,
    context.contractStateKey
  ).then((s1) => {
    context.writeMessage(s1);
  });
}

class MessageError extends Error {
  constructor(message) {
    super(message);
    this.name = "MessageError";
  }
}

const DEFAULT_CACHE_LIFETIME = 5000; //ms
class StateCache {
  constructor() {
    this._state = {};
  }
  add(obj, lifetime) {
    let key;
    do {
      key = util.bytesToHex(random.getBytesSync(32));
    } while (this.has(key));
    this._state[key] = obj;
    if (typeof lifetime === "number") {
      setTimeout(() => {
        this.remove(key);
      }, lifetime);
    } else if (lifetime !== "infinite") {
      setTimeout(() => {
        this.remove(key);
      }, DEFAULT_CACHE_LIFETIME);
    }
    vl(_moduleName, `Server Cache: Key ${key.substr(0, 4)}... added`);
    return key;
  }

  has(key) {
    return key in this._state;
  }

  remove(key) {
    vl(_moduleName, `Server Cache: Key ${key.substr(0, 4)}... removed`);
    return delete this._state[key];
  }

  get(key) {
    vl(_moduleName, `Server Cache: Key ${key.substr(0, 4)}... requested`);
    return this._state[key];
  }
}

let _config = {
  // factcheck.js config file.
  useTls: true,
  clientEnforceTls: true,
  verbose: false,
  enableCaching: true,
  cacheLifetime: 1000,
  maximumTimestampAge: 60,
  pki: {
    privateKey: {
      type: "",
      format: "",
      path: "",
    },

    ownCert: {
      type: "",
      format: "",
      path: "",
    },
    caCertPath: "",
  },
};

module.exports.get = () => _config;

module.exports.set = (config) => {
  _config = config;
};

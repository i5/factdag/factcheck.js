# factcheck.js

A library for Node.js, written in JavaScript for creating digital transmission contracts to ensure immutability of exchanged data.

Unstable Proof-of-Concept implementation, use at own risk.

Designed to work together with [factlib.js](https://git.rwth-aachen.de/i5/factdag/factlibjs).

## Supplementary Material

A comparative overview of related work can be found [here](https://git.rwth-aachen.de/i5/factdag/factcheck.js/-/raw/relwork-overview/relwork-overview/overview.pdf).

## Installation

To use `factcheck.js` in your own project, first setup the `i5` registry using

```bash
echo @i5:registry=https://git.rwth-aachen.de/api/v4/packages/npm/ >> .npmrc
```

Then, you can install `factcheck.js` by calling

```bash
npm i @i5/factcheck.js
```

## Recommended Additional Libraries

Modules which are not directly required by `factcheck.js`, but are useful for integrating `factcheck.js`, can be found in the `devDependencies`.
Usage is demonstrated in the examples.

## Usage & Config

`factcheck.js` exports four submodules: `server`, `client`, `facts`, `verify`, as well as two methods `setConfig` and `init`.
A config can be specified by passing a JS object via the `setConfig` function.
We encourage to use the default `JSON-C` config template, which can be found in the examples as well.
To learn about usage, inspect the examples included in the library.

## Example Setup

### Clone the repo and run npm install

```bash
git clone git@git.rwth-aachen.de:i5/factdag/factcheck.js.git; cd factcheck.js; npm i
```

### (Optional) Setup PKI files

By now, example certificates and keys in the `pki` directory are used by client and server.

For now, these can simply be exchanged by your own. Later, a config file will be added to configure your own certificates and keys.

### Run the TLS example

First, start the example server:

```bash
node examples/tls/server-example.js
```

Then, start the example client in a second console:

```bash
node examples/tls/client-example.js
```

In the console outputs now should both show the generated contracts.

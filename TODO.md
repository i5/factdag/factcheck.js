# TODOs for the Prototype Codebase

- Add support for other crypto algorithms

  - Signatures (Ed25519? DSS?)
  - Certificate Types (PGP? What for Ed25519?)
  - Checksums
  - Configurable!

- TypeScript-ize all the things!

- Documentation

  - OpenAPI?
  - TypeDoc? (https://github.com/TypeStrong/typedoc)

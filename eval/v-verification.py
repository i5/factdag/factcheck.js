import os
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib
from datetime import datetime
import dateutil

scriptpath = os.path.dirname(sys.argv[0])

df = pd.read_csv(os.path.join(scriptpath, "verification.csv"))
default_figsize = (9, 3)


def show():
    #fig, ax = plt.subplots(ncols=3, figsize=default_figsize)

    fig = []
    ax = []
    for i in range(3):
        [_fig, _ax] = plt.subplots(
            figsize=[default_figsize[0]/3, default_figsize[1]])
        fig.append(_fig)
        ax.append(_ax)

    # 1: Duration by format
    df_r = df[df["parallel_contracts"] ==
              1][df["facts_per_contract"] == 50][df["delay"] == 0]
    data = []
    for f in df_r["format"].unique():
        data.append(df_r[df_r["format"] == f]["contract_duration"])
    ax[0].boxplot(data)
    ax[0].set_xticklabels(["JSON", "JSON-LD\ncompact", "JSON-LD\nflattened"])
    ax[0].set_xlabel("Contract format")
    ax[0].grid(color="lightgrey", axis="y")

    ("Sizes for format:")
    for fmt in df_r["format"].unique():
        print(fmt, df_r[df_r["format"] == fmt].shape[0])

    # 2: Duration by facts
    df_r = df[df["parallel_contracts"] ==
              1][df["format"] == "json"][df["delay"] == 0]
    agged = df_r.groupby("facts_per_contract").agg(
        {"contract_duration": ["std", "mean"]})
    x = agged.index
    y = agged["contract_duration"]["mean"]
    # 2*std for ~95% confidence interval
    yerr = agged["contract_duration"]["std"] * 2
    ax[1].errorbar(x, y, yerr, capsize=5.0, capthick=1.5)
    ax[1].set_xlim(0, 110)
    ax[1].set_xticks([0, 10, 20, 30, 40, 50, 75, 100])
    ax[1].set_xlabel("Facts per contract")
    ax[1].grid(color="lightgrey")

    print("Sizes for #facts:")
    for fmt in df_r["facts_per_contract"].unique():
        print(fmt, df_r[df_r["facts_per_contract"] == fmt].shape[0])

    # 3: Duration by delay
    df_r = df[df["parallel_contracts"] ==
              1][df["format"] == "json"][df["facts_per_contract"] == 50]
    agged = df_r.groupby("delay").agg({"contract_duration": ["mean", "std"]})
    x = agged.index
    y = agged["contract_duration"]["mean"]
    # 2*std for ~95% confidence interval
    yerr = agged["contract_duration"]["std"] * 2
    coef = np.polyfit(df_r["delay"], df_r["contract_duration"], 1)
    ax[2].errorbar(x, y, yerr, capsize=5.0, capthick=1.5,
                   label="Slope: %.2f" % coef[0])
    ax[2].legend()
    ax[2].set_xlabel("Artificial route delay [ms]")
    ax[2].grid(color="lightgrey")

    print("Sizes for delay:")
    for fmt in df_r["delay"].unique():
        print(fmt, df_r[df_r["delay"] == fmt].shape[0])

    ax[0].set_ylabel("Verification time [ms]")
    for _ax in ax:
        _ax.spines['right'].set_visible(False)
        _ax.spines['top'].set_visible(False)

    i = 0
    for _fig in fig:
        _fig.savefig(os.path.join(scriptpath, "VerificationFig-%d.pdf" % i),
                     bbox_inches="tight")
        i += 1
    plt.show()


show()

# df_r = df[df["parallel_contracts"] ==
#          1][df["delay"] == 0]
# print(df_r)
# y = df_r["contract_duration"]
# plt.scatter(df_r["facts_per_contract"], y)
# plt.show()

const net = require("net");

module.exports.createProxy = (inPort, outPort, artificialDelay) => {
  let state = {};

  let delayEnabled =
    artificialDelay &&
    typeof artificialDelay === "number" &&
    artificialDelay > 0;

  let server = net.createServer((inSocket) => {
    let inRemotePort = inSocket.remotePort;
    state[inRemotePort] = {
      inToOutBytes: 0,
      outToInBytes: 0,
    };

    let outSocket = net.createConnection(outPort);

    inSocket.on("data", (data) => {
      //console.log("Bytes counted (in->out): " + data.length);
      state[inRemotePort].inToOutBytes += data.length;
      if (delayEnabled) {
        setTimeout(() => {
          outSocket.write(data);
        }, artificialDelay);
      } else {
        outSocket.write(data);
      }
    });

    outSocket.on("data", (data) => {
      state[inRemotePort].outToInBytes += data.length;
      //console.log("Bytes counted (out->in): " + data.length);
      if (delayEnabled) {
        setTimeout(() => {
          inSocket.write(data);
        }, artificialDelay);
      } else {
        inSocket.write(data);
      }
    });
  });

  server.on("error", (err) => {
    throw err;
  });

  server.listen(inPort);

  state.server = server;

  return state;
};

import os
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib
from datetime import datetime
import dateutil

scriptpath = os.path.dirname(sys.argv[0])

df = pd.read_csv(os.path.join(scriptpath, "generation.csv"))
default_figsize = (9, 4)


def perc5err(x):
    return np.percentile(a=x, q=5)-np.mean(x)


def perc95err(x):
    return np.percentile(a=x, q=95)-np.mean(x)


def showDurationByNumFactsAndMode():
    df_nf = df[df["cert_chain_len"] == 1][["tls", "http",
                                           "num_facts", "total_duration", "bytes_total"]]
    agg_dict = {
        "total_duration": ["min", "max", "mean", np.std, perc5err, perc95err],
        "bytes_total": "mean",
    }
    df_nf_0_0 = df_nf[(df_nf["tls"] == 0) & (
        df_nf["http"] == 0)].groupby(by="num_facts").agg(agg_dict)
    df_nf_0_1 = df_nf[(df_nf["tls"] == 0) & (
        df_nf["http"] == 1)].groupby(by="num_facts").agg(agg_dict)
    df_nf_1_0 = df_nf[(df_nf["tls"] == 1) & (
        df_nf["http"] == 0)].groupby(by="num_facts").agg(agg_dict)
    df_nf_1_1 = df_nf[(df_nf["tls"] == 1) & (
        df_nf["http"] == 1)].groupby(by="num_facts").agg(agg_dict)

    print(df_nf_0_0)

    # print(df_nf_0_0)
    # plt.scatter(x=df_nf_0_0.index, y=[df_nf_0_0["total_duration"]["mean"], df_nf_0_1["total_duration"]
    #                                  ["mean"], df_nf_1_0["total_duration"]["mean"], df_nf_1_1["total_duration"]["mean"]])

    # x = df_nf_0_0.index
    # y1 = df_nf_0_0["total_duration"]["mean"]
    # y2 = df_nf_0_1["total_duration"]["mean"]
    # y3 = df_nf_1_0["total_duration"]["mean"]
    # y4 = df_nf_1_1["total_duration"]["mean"]

    df2 = [[None, None], [None, None]]

    for tls in [0, 1]:
        for http in [0, 1]:
            df2[tls][http] = df[df["tls"] == tls][df["http"] == http]

    fig = plt.figure()
    ax1 = fig.add_subplot(111)

    ax1.errorbar(x=df_nf_0_0.index, y=df_nf_0_0["total_duration"]
                 ["mean"], yerr=df_nf_0_0["total_duration"]["std"], c="r", label="disabled")
    ax1.errorbar(x=df_nf_1_0.index, y=df_nf_1_0["total_duration"]
                 ["mean"], yerr=df_nf_1_0["total_duration"]["std"], c="b", label="disabled")

    # ax1.scatter(x, y4, c='y', marker="s", label='HTTPS')
    plt.legend(loc='upper right', title="TLS")
    plt.show()

# showDurationByNumFactsAndMode()


def showBytesByNumFacts():
    # TODO: Fix mode, show correlation coefficient and regression parameters

    df_cl_1 = df[df["cert_chain_len"] == 1]

    fig, (ax0, ax1) = plt.subplots(nrows=2, sharex="all")

    ################## PER-CONTRACT ##################
    # Plot contract sizes
    df_tmp = df_cl_1.groupby("num_facts").agg(
        {"contract_size": "mean"})
    x = df_tmp.index
    y = df_tmp["contract_size"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    ax0.plot(x, y, "s", color="tab:green",
             label="Contract size (Slope: %.0f B/fact)" % (coef[0]))
    ax0.plot(x, reg(x), "--", color="tab:green")

    min_theo = coef[0]

    # Plot total bytes by mode,
    agg_dict = {"bytes_total": "mean"}

    x = df_cl_1.groupby("num_facts").agg("min").index
    y_0 = df_cl_1[df_cl_1["http"] == 0]
    y_0_0 = y_0[y_0["tls"] == 0].groupby(
        "num_facts").agg(agg_dict)["bytes_total"]
    y_0_1 = y_0[y_0["tls"] == 1].groupby(
        "num_facts").agg(agg_dict)["bytes_total"]
    y_1 = df_cl_1[df_cl_1["http"] == 1]
    y_1_0 = y_1[y_1["tls"] == 0].groupby(
        "num_facts").agg(agg_dict)["bytes_total"]
    y_1_1 = y_1[y_1["tls"] == 1].groupby(
        "num_facts").agg(agg_dict)["bytes_total"]

    coef = np.polyfit(x, y_0_0, 1)
    reg = np.poly1d(coef)
    ax0.plot(x, y_0_0, "s", color="tab:orange",
             label="Total Bytes sent (TCP) (Slope: %.0f B/fact)" % (coef[0]))
    ax0.plot(x, reg(x), "--", color="tab:orange")

    coef = np.polyfit(x, y_0_1, 1)
    reg = np.poly1d(coef)
    ax0.plot(x, y_0_1, "s", color="tab:red",
             label="Total Bytes sent (TCP+TLS)")
    ax0.plot(x, reg(x), "--", color="tab:red")

    coef = np.polyfit(x, y_1_0, 1)
    reg = np.poly1d(coef)
    ax0.plot(x, y_1_0, "s", color="tab:blue",
             label="Total Bytes sent (HTTP)")
    ax0.plot(x, reg(x), "--", color="tab:blue")

    coef = np.polyfit(x, y_1_1, 1)
    reg = np.poly1d(coef)
    ax0.plot(x, y_1_1, "s", color="tab:purple",
             label="Total Bytes sent (HTTPS)")
    ax0.plot(x, reg(x), "--", color="tab:purple")

    ax0.legend(loc="upper left")
    ax0.set_ylim(0)
    ax0.set_xticks(x)
    ax0.grid(color="lightgrey")
    locs = ax0.get_yticks()
    labels = list(map(lambda loc: "%.0fK" % np.floor(loc/1000), locs))
    ax0.set_yticklabels(labels=labels)
    ax0.set_ylabel("Bytes")
    plt.xlabel("facts per contract")

    # Hide the right and top spines
    ax0.spines['right'].set_visible(False)
    ax0.spines['top'].set_visible(False)

    ################## PER-FACT ##################

    # Plot Storage bytes
    df_tmp = df_cl_1.groupby("num_facts").agg(
        {"contract_size": "mean"})

    x = df_tmp.index
    y = df_tmp["contract_size"] / df_tmp.index
    ax1.plot(x, y, "s", color="tab:green",
             label="Contract B/fact")

    ax1.hlines(min_theo, 0, max(x), 'tab:green', "dotted",
               "Theoretical minimum\n(%.0f Contract B/fact)" % min_theo)

    # Plot total bytes by mode,
    agg_dict = {"bytes_total": "mean"}

    x = df_cl_1.groupby("num_facts").agg("min").index
    y_0 = df_cl_1[df_cl_1["http"] == 0]
    y_0_0 = y_0[y_0["tls"] == 0].groupby(
        "num_facts").agg(agg_dict)
    y_0_0 = y_0_0["bytes_total"]/y_0_0.index
    y_0_1 = y_0[y_0["tls"] == 1].groupby(
        "num_facts").agg(agg_dict)
    y_0_1 = y_0_1["bytes_total"]/y_0_1.index

    y_1 = df_cl_1[df_cl_1["http"] == 1]
    y_1_0 = y_1[y_1["tls"] == 0].groupby(
        "num_facts").agg(agg_dict)
    y_1_0 = y_1_0["bytes_total"]/y_1_0.index

    y_1_1 = y_1[y_1["tls"] == 1].groupby(
        "num_facts").agg(agg_dict)
    y_1_1 = y_1_1["bytes_total"]/y_1_1.index

    ax1.plot(x, y_0_0, "s", color="tab:orange",
             label="Bytes sent/fact(TCP)")

    ax1.plot(x, y_0_1, "s", color="tab:red",
             label="Bytes sent/fact (TCP+TLS)")

    ax1.plot(x, y_1_0, "s", color="tab:blue",
             label="Bytes sent/fact (HTTP)")

    ax1.plot(x, y_1_1, "s", color="tab:purple",
             label="Bytes sent/fact (HTTPS)")

    ax1.legend(loc="upper right")
    ax1.set_ylim(10, 100000)
    ax1.set_yscale("log")
    ax1.set_xticks(x)
    ax1.grid(color="lightgrey")
    locs = ax1.get_yticks()
    labels = list(map(lambda loc: "%.0fK" % np.floor(loc/1000)
                      if loc >= 1000 else "%.0f" % loc, locs))
    ax1.set_yticklabels(labels=labels)
    ax1.set_ylabel("Bytes")
    plt.xlabel("facts per contract")

    # Hide the right and top spines
    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)

    plt.xlim(0)
    plt.show()


showBytesByNumFacts()


def showTimeByMode():
    fig = plt.figure()

    df_0_0 = df[(df["tls"] == 0) & (
        df["http"] == 0)]
    df_0_1 = df[(df["tls"] == 0) & (
        df["http"] == 1)]
    df_1_0 = df[(df["tls"] == 1) & (
        df["http"] == 0)]
    df_1_1 = df[(df["tls"] == 1) & (
        df["http"] == 1)]

    plt.boxplot([df_0_0["total_duration"], df_0_1["total_duration"],
                 df_1_0["total_duration"], df_1_1["total_duration"]])

    plt.xticks(range(1, 5), ["TCP", "TCP+TLS", "HTTP", "HTTPS"])
    plt.xlabel("Handshake mode")
    plt.ylabel("Handshake duration [ms]")

    plt.grid(color="lightgrey")

    ax0 = plt.axes()
    ax0.spines['right'].set_visible(False)
    ax0.spines['top'].set_visible(False)


# showTimeByMode()

def std2(a):
    return 2.0*np.std(a)


def std_alt(a):
    return np.std(a)


def showDelayEffects():
    # Version 1: 4 subplots with shared axes
    # Version 2: All in one plot
    _ver = 2

    fig = plt.figure(figsize=default_figsize)
    ax_common = fig.add_subplot(111)

    if _ver == 1:
        axes = fig.subplots(
            ncols=2, nrows=2, sharex="all", sharey="all")
        ax_common.spines['top'].set_color('none')
        ax_common.spines['bottom'].set_color('none')
        ax_common.spines['left'].set_color('none')
        ax_common.spines['right'].set_color('none')
        ax_common.tick_params(labelcolor='w', top=False,
                              bottom=False, left=False, right=False)
    elif _ver == 2:
        axes = [[ax_common, ax_common], [ax_common, ax_common]]

    df_0_0 = df[(df["tls"] == 0) & (
        df["http"] == 0)].groupby("artificial_delay").agg(
        {"total_duration": ["mean", "std"]})
    df_0_1 = df[(df["tls"] == 1) & (
        df["http"] == 0)].groupby("artificial_delay").agg(
        {"total_duration": ["mean", "std"]})
    df_1_0 = df[(df["tls"] == 0) & (
        df["http"] == 1)].groupby("artificial_delay").agg(
        {"total_duration": ["mean", "std"]})
    df_1_1 = df[(df["tls"] == 1) & (
        df["http"] == 1)].groupby("artificial_delay").agg(
        {"total_duration": ["mean", "std"]})

    print(df_0_0)

    print(df_0_0["total_duration"]["std"]*2)

    print(df[df["tls"] == 0][df["http"] == 0]
          [df["artificial_delay"] == 0]["total_duration"])

    x = df_0_0.index
    y = df_0_0["total_duration"]["mean"]
    yerr = 2*df_0_0["total_duration"]["std"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    slope_label = "Slope: %.2f" % (coef[0])
    label = slope_label if _ver == 1 else "TCP (%s)" % slope_label
    axes[0][0].errorbar(x, y, yerr, color="tab:orange",
                        label=label)
    if _ver == 1:
        axes[0][0].set_title("TCP")

    x = df_0_1.index
    y = df_0_1["total_duration"]["mean"]
    yerr = 2*df_0_1["total_duration"]["std"]
    slope_label = "Slope: %.2f" % (coef[0])
    label = slope_label if _ver == 1 else "TLS (%s)" % slope_label
    axes[0][1].errorbar(x, y, yerr, color="tab:red",
                        label=label)
    if _ver == 1:
        axes[0][1].set_title("TCP+TLS")

    x = df_1_0.index
    y = df_1_0["total_duration"]["mean"]
    yerr = 2*df_1_0["total_duration"]["std"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    slope_label = "Slope: %.2f" % (coef[0])
    label = slope_label if _ver == 1 else "HTTP (%s)" % slope_label
    axes[1][0].errorbar(x, y, yerr, color="tab:blue",
                        label=label)
    if _ver == 1:
        axes[1][0].set_title("HTTP")

    x = df_1_1.index
    y = df_1_1["total_duration"]["mean"]
    yerr = 2*df_1_1["total_duration"]["std"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    slope_label = "Slope: %.2f" % (coef[0])
    label = slope_label if _ver == 1 else "HTTPS (%s)" % slope_label
    axes[1][1].errorbar(x, y, yerr, color="tab:purple",
                        label=label)
    if _ver == 1:
        axes[1][1].set_title("HTTPS")

    for _axes in axes:
        for ax in _axes:
            ax.legend(loc="upper left")
            ax.grid(color="lightgrey")
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.set_xlim(0)
            ax.set_ylim(0)

    if _ver == 2:
        ax_common.grid(color="lightgray")

    ax_common.set_xlabel("Simulated delay [ms]")
    ax_common.set_ylabel("Handshake duration [ms]")

    plt.savefig(os.path.join(scriptpath, "ArtificialDelayFig.pdf"),
                bbox_inches="tight")
    plt.show()


# showDelayEffects()


#datetimes = df["start_time"].map(lambda str: dateutil.parser.isoparse(str))
#dates = matplotlib.dates.date2num(datetimes)
# plt.scatter(df["num_facts"], df["contract_size"])
# plt.show()

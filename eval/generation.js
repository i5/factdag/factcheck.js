// NOTE: Disabling Wi-Fi may help with mitigating TCP connection problems
//
// Features:
//   - run_id
//   - start_time
//   - tls
//   - http
//   - cert_chain_len
//   - num_facts
//   - client_duration
//   - total_duration
//   total_message_bytes
//
// Eval module config, to be changed according to experiment

const checksums = require("../lib/crypto/checksums");
const factcheck = require("../factcheck");
const fs = require("fs");
const pki = require("../lib/crypto/pki");
const tcpProxy = require("./tcpProxy");
const { random, util } = require("node-forge");

const iriBase = "http://example.com/";
function makeIRI() {
  return iriBase + util.bytesToHex(random.getBytesSync(32));
}
function makeData() {
  return random.getBytesSync(1024);
}

var program = [];

if (true) {
  // Late data added to dataset-1.csv for further evaluation of CCL
  [1, 2, 3, 4, 4, 5, 5].forEach((ccl) => {
    program.push({
      tls: true,
      http: true,
      numFacts: 10,
      certChainLen: ccl,
      maxRounds: 20,
      interRoundTime: 1000,
      saveContracts: false,
      addData: true,
      artificialDelay: 0,
    });
  });
}

if (false) {
  [4, 5];
  program.push({
    tls: false,
    http: true,
    numFacts: 20,
    certChainLen: 1,
    maxRounds: 20,
    interRoundTime: 1000,
    saveContracts: true,
    addData: true,
    artificialDelay: 0,
  });
}

if (false) {
  // One long run without HTTP and TLS, to see how duration changes over time
  program.push({
    tls: false,
    http: false,
    numFacts: 100,
    certChainLen: 2,
    maxRounds: 200,
    interRoundTime: 1000,
    artificialDelay: 0,
  });
}

if (false) {
  // Evaluation of artificial delay, with long inter-round time to avoid errors
  [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100].forEach((artificialDelay) => {
    [false, true].forEach((tls) => {
      [false, true].forEach((http) => {
        [false, true].forEach((statefulMode) => {
          program.push({
            tls,
            http,
            numFacts: 10,
            certChainLen: 1,
            maxRounds: 10,
            interRoundTime: 5 * artificialDelay + 1000,
            saveContracts: false,
            artificialDelay,
            statefulMode,
          });
        });
      });
    });
  });
}

if (false) {
  // Large run with broad parameter space (about 2.5h)
  // c.v. Large1
  [false, true].forEach((http) => {
    [false, true].forEach((tls) => {
      [1, 2, 3].forEach((certChainLen) => {
        [1, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100].forEach((numFacts) => {
          [false, true].forEach((statefulMode) => {
            program.push({
              tls,
              http,
              numFacts,
              certChainLen,
              maxRounds: 20,
              interRoundTime: 1000,
              saveContracts: true,
              statefulMode,
            });
          });
        });
      });
    });
  });
}

if (false) {
  // Modified large run with broad parameter space, without certChainLen eval
  // c.v. Large1
  [false, true].forEach((http) => {
    [false, true].forEach((tls) => {
      [false, true].forEach((statefulMode) => {
        [1, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100].forEach((numFacts) => {
          program.push({
            tls,
            http,
            numFacts,
            certChainLen: 1,
            maxRounds: 5,
            interRoundTime: 1000,
            saveContracts: true,
            statefulMode,
          });
        });
      });
    });
  });
}

if (true) {
  // shuffle the program
  shuffle(program);
}

if (true) {
  // Empty run at the beginning to avoid unusually long first run
  // Shorter inter-round time can be tolerated, as tls and http are off
  program.unshift({
    tls: false,
    http: false,
    numFacts: 1,
    certChainLen: 2,
    maxRounds: 20,
    interRoundTime: 1000,
    saveContracts: false,
    addData: false,
  });
}

const absoluteStart = new Date();
runProgramFrom(0);

async function runProgramFrom(stepIndex) {
  if (stepIndex >= program.length) {
    tcpProxyState.server.close();
    writeStream.close();
    return;
  }

  let evalConfig = program[stepIndex];

  const timePassed = new Date() - absoluteStart;
  const timePerStep = timePassed / (stepIndex + 1);
  const timeLeft = Math.floor(
    (timePerStep * (program.length - stepIndex)) / 1000
  );

  console.log(
    `\nStarting program step ${stepIndex + 1} of ${program.length}.\n` +
      `Approximate remaining time: ${Math.floor(timeLeft / 60)} min ${
        timeLeft % 60
      } sec` +
      `Configuration:\n${JSON.stringify(evalConfig, null, 2)}\n`
  );

  // Create run_id
  let runId = checksums.md5sum(Date.now());

  if (evalConfig.saveContracts === true) {
    // Create contract directory
    fs.mkdir("./eval/contracts/" + runId, { recursive: true }, (err, path) => {
      if (err) {
        throw err;
      }
    });
  }

  // Load config
  let config = {
    useTls: evalConfig.tls,
    clientEnforceTls: evalConfig.tls,
    verbose: false,
    pki: {
      privateKey: { type: "rsa", format: "pem", path: "./pki/chain/priv.pem" },
      ownCert: {
        type: "PKCS7",
        format: "pem",
        path: "./pki/chain/chain-pkcs7.pem",
      },
      caCertPath: "./pki/ca",
    },
    independentAgents: true,
    enableCaching: true,
  };

  factcheck.setConfig(config);

  // Generate fresh certificate chain
  const { execSync } = require("child_process");

  execSync(`bash ./pki/chain/makeChain.sh ${evalConfig.certChainLen}`);

  let roundData = {
    data: {
      i: 1,
      runId: runId,
      startTime: undefined,
      tls: evalConfig.tls,
      http: evalConfig.http,
      certChainLen: evalConfig.certChainLen,
      numFacts: evalConfig.numFacts,
      clientDuration: undefined,
      totalDuration: undefined,
      totalMessageBytes: 0,
      clientPorts: [],
    },
    evalConfig,
  };

  factcheck.init().then(() => {
    startServer(roundData);

    var roundsDone = 0;

    function repeatedClientIteration() {
      if (roundsDone < evalConfig.maxRounds) {
        roundsDone++;
        console.log(
          `Starting round ${roundsDone} of ${evalConfig.maxRounds}...`
        );
        roundData.data = {
          i: roundsDone,
          runId: runId,
          startTime: undefined,
          tls: config.useTls,
          http: evalConfig.http,
          certChainLen: evalConfig.certChainLen,
          numFacts: evalConfig.numFacts,
          clientDuration: undefined,
          totalDuration: undefined,
          totalMessageBytes: 0,
          clientPorts: [],
        };
        clientIteration(roundData);
        setTimeout(() => {
          repeatedClientIteration();
        }, evalConfig.interRoundTime);
      } else {
        setTimeout(() => {
          runProgramFrom(stepIndex + 1);
        }, evalConfig.interRoundTime);
      }
    }
    repeatedClientIteration();
  });
}

let tcpProxyState;

function startServer(roundData) {
  let { evalConfig } = roundData;

  let serverIRI = makeIRI();

  const { server, facts } = factcheck;
  facts.addRoute({
    priority: 0,
    appliesTo: (factID) => true,
    get: (factID) => [factID, "string"],
  });

  let close = () => {};

  if (evalConfig.http) {
    const express = require("express");
    const http = require("http");
    const https = require("https");

    const factcheckMiddleware = factcheck.server.expressMiddleware({
      onContract: handleContract,
      onError: handleError,
      contractIRICallback(contract) {
        return makeIRI();
      },
      authID: serverIRI,
      supportStatefulMode: evalConfig.statefulMode,
    });

    // Setup TLS credentials
    let credentials = {
      key: pki.getPrivateKey("pem"),
      cert: pki.getCertificateChain("pem"),
    };

    const app = express();
    const bodyParser = require("body-parser");
    app.use(bodyParser.json());
    app.post("/factcheckAPI", factcheckMiddleware);

    let httpServer = http.createServer(app);
    httpServer.listen(8080);

    let httpsServer = https.createServer(credentials, app);
    httpsServer.listen(8443);

    close = () => {
      httpServer.close();
      httpsServer.close();
      //runningChecker();f
    };

    console.log("Server listening on 8080 (HTTP) and 8443 (HTTPS)...");
  } else {
    console.log("Creating server instance...");
    const serverInstance = server.create(evalConfig.tls ? 8443 : 8080, {
      useTls: evalConfig.tls,
      onContract: handleContract,
      onError: handleError,
      contractIRICallback(contract) {
        return makeIRI();
      },
      authID: serverIRI,
      supportStatefulMode: evalConfig.statefulMode,
    });
    console.log(
      `Server listening on port ${evalConfig.tls ? 8443 : 8080} (${
        evalConfig.tls ? "TCP+TLS" : "TCP"
      })...`
    );

    close = () => {
      serverInstance.close();
    };
  }

  // If proxy is running, stop it to create a new one
  if (tcpProxyState) {
    tcpProxyState.server.close();
  }

  //Start TCP proxy
  tcpProxyState = tcpProxy.createProxy(
    8000,
    evalConfig.tls ? 8443 : 8080,
    evalConfig.artificialDelay
  );

  function handleContract(contract) {
    const serverDoneTime = new Date();
    roundData.data.totalDuration = serverDoneTime - roundData.data.startTime;
    roundData.data.contractSize = contract.getContent("json").length;
    setTimeout(() => {
      // Correct byte numbers should be there
      // All data is collected here => make new entry to csv
      let ports = roundData.data.clientPorts;
      let bytes =
        ports.length > 0
          ? ports
              .map((port) => tcpProxyState[port])
              .reduce((a, b) => ({
                inToOutBytes: a.inToOutBytes + b.inToOutBytes,
                outToInBytes: a.outToInBytes + b.outToInBytes,
              }))
          : { inToOutBytes: 0, outToInBytes: 0 };

      ports.forEach((port) => {
        setTimeout(() => {
          delete tcpProxyState[port];
        }, 5000);
      });

      roundData.data.clientToServerBytes = bytes.inToOutBytes;
      roundData.data.serverToClientBytes = bytes.outToInBytes;

      addData(roundData);
      if (roundData.data.i === evalConfig.maxRounds) {
        close();
      }
    }, 100);
    //If enabled, save contract as file
    if (evalConfig.saveContracts) {
      fs.writeFile(
        `./eval/contracts/${roundData.data.runId}/${roundData.data.i}.json`,
        contract.getContent("json"),
        (err) => {
          if (err) {
            console.log("WriteFile err:\n" + err);
          }
        }
      );
      contract.getLDContent("jsonld").then((ldContract) => {
        fs.writeFile(
          `./eval/contracts/${roundData.data.runId}/${roundData.data.i}.jsonld`,
          ldContract,
          (err) => {
            if (err) {
              console.log("WriteFile err:\n" + err);
            }
          }
        );
      });
    }
  }

  function handleError(err) {
    console.log("Error creating contract:");
    console.log(err);
  }
}

function clientIteration(roundData) {
  let { evalConfig } = roundData;

  console.log("Starting contract generation...");

  // Choose factIDs
  let factIDs = [];
  for (i = 0; i < evalConfig.numFacts; i++) {
    factIDs.push(makeIRI());
  }

  let mode = "raw";
  if (evalConfig.http) {
    mode = evalConfig.tls ? "https" : "http";
  }

  roundData.data.startTime = new Date();
  factcheck.client.requestContract(factIDs, {
    mode,
    port: 8000,
    host: "localhost",
    path: "/factcheckAPI",
    onError: handleError,
    onContract: handleContract,
    evalPortCallback: (port) => {
      roundData.data.clientPorts.push(port);
    },
    supportStatefulMode: evalConfig.statefulMode,
  });

  function handleContract(contract) {
    const clientDoneTime = new Date();
    roundData.data.clientDuration = clientDoneTime - roundData.data.startTime;
  }

  function handleError(err) {
    console.log("Error requesting contract: " + err);
  }
}

const writeStream = fs.createWriteStream("./eval/generation.csv", {
  flags: "a",
});

function addData(roundData) {
  let { evalConfig } = roundData;

  if (evalConfig.addData !== false) {
    console.log(
      `Round ${roundData.data.i}/${evalConfig.maxRounds} done, adding result to CSV`
    );
    let {
      runId,
      startTime,
      tls,
      http,
      certChainLen,
      numFacts,
      clientDuration,
      totalDuration,
      clientToServerBytes,
      serverToClientBytes,
      contractSize,
    } = roundData.data;
    const artificialDelay = roundData.evalConfig.artificialDelay || 0;
    const statefulMode = roundData.evalConfig.statefulMode ? 1 : 0;
    writeStream.write(
      `${runId},${startTime.toISOString()},${tls ? 1 : 0},${
        http ? 1 : 0
      },${certChainLen},${numFacts},${clientDuration},` +
        `${totalDuration},${clientToServerBytes},${serverToClientBytes},` +
        `${clientToServerBytes + serverToClientBytes},${contractSize},` +
        `${artificialDelay},${statefulMode}\n`
    );
  } else {
    console.log(
      `Round ${roundData.data.i}/${evalConfig.maxRounds} done, results are ignored`
    );
  }
}

function shuffle(array) {
  var currentIndex = array.length,
    temporaryValue,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

import os
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib
from datetime import datetime
import dateutil

default_figsize = (9, 3)

scriptpath = os.path.dirname(sys.argv[0])

df = pd.read_csv(os.path.join(scriptpath, "generation-1.csv"))

df_delay = pd.read_csv(os.path.join(scriptpath, "generation-2.csv"))


def dfStats(df):
    print("\n\nDF Stats: (Unique Values)\n")
    for key in df.keys():
        g = df.groupby(key).count()
        if key == "run_id":
            print("Runs: %d" % (g.shape[0]))
            print("Per run: %d" % (g.iloc[0, 0]))
        elif key in ["start_time", "client_duration", "total_duration"]:
            print(end="")
        else:
            print(g.iloc[:, 0])

    print()


def showDurationByNumFactsAndMode():
    df_nf = df[df["cert_chain_len"] == 2][["tls", "http",
                                           "num_facts", "total_duration", "bytes_total"]]
    agg_dict = {
        "total_duration": ["min", "max", "mean", np.std],
        "bytes_total": "mean",
    }

    df_nf_0_0 = df_nf[(df_nf["tls"] == 0) & (
        df_nf["http"] == 0)].groupby(by="num_facts").agg(agg_dict)
    df_nf_0_1 = df_nf[(df_nf["tls"] == 0) & (
        df_nf["http"] == 1)].groupby(by="num_facts").agg(agg_dict)
    df_nf_1_0 = df_nf[(df_nf["tls"] == 1) & (
        df_nf["http"] == 0)].groupby(by="num_facts").agg(agg_dict)
    df_nf_1_1 = df_nf[(df_nf["tls"] == 1) & (
        df_nf["http"] == 1)].groupby(by="num_facts").agg(agg_dict)

    # print(df_nf_0_0)
    # plt.scatter(x=df_nf_0_0.index, y=[df_nf_0_0["total_duration"]["mean"], df_nf_0_1["total_duration"]
    #                                  ["mean"], df_nf_1_0["total_duration"]["mean"], df_nf_1_1["total_duration"]["mean"]])

    # x = df_nf_0_0.index
    # y1 = df_nf_0_0["total_duration"]["mean"]
    # y2 = df_nf_0_1["total_duration"]["mean"]
    # y3 = df_nf_1_0["total_duration"]["mean"]
    # y4 = df_nf_1_1["total_duration"]["mean"]

    df2 = [[None, None], [None, None]]

    for tls in [0, 1]:
        for http in [0, 1]:
            df2[tls][http] = df[df["tls"] == tls][df["http"] == http]

    fig = plt.figure()
    ax1 = fig.add_subplot(111)

    ax1.errorbar(x=df_nf_0_0.index, y=df_nf_0_0["total_duration"]
                 ["mean"], yerr=df_nf_0_0["total_duration"]["std"], c="r", label="disabled")
    ax1.errorbar(x=df_nf_1_0.index, y=df_nf_1_0["total_duration"]
                 ["mean"], yerr=df_nf_1_0["total_duration"]["std"], c="b", label="disabled")

    # ax1.scatter(x, y4, c='y', marker="s", label='HTTPS')
    plt.legend(loc='upper right', title="TLS")
    plt.show()

# showDurationByNumFactsAndMode()


def showBytesByMode():
    fig = plt.figure(figsize=default_figsize)
    ax = plt.subplot(111)

    df_r = df[df["cert_chain_len"] ==
              2][df["num_facts"] == 10][df["artificial_delay"] == 0]

    dfStats(df_r)

    df_r["mode_key"] = 4*df_r["stateful_mode"]+2*df_r["tls"]+df_r["http"]
    grouped = df_r.groupby("mode_key").agg(
        {"bytes_s_c": ["mean", "std"], "bytes_c_s": ["mean", "std"]})
    ind = np.arange(8)

    y_s_c = grouped["bytes_s_c"]["mean"]
    y_c_s = grouped["bytes_c_s"]["mean"]

    ax.bar(ind, y_s_c, color="tab:blue", label="Server to client")
    ax.bar(ind, y_c_s, bottom=y_s_c, color="tab:red", label="Client to server")

    ax.set_xlabel("Handshake mode (*: stateful)")
    ax.set_ylabel("Data Transmission [kB]")
    ax.set_xticklabels(["", "TCP", "HTTP", "TLS", "HTTPS",
                        "TCP*", "HTTP*", "TLS*", "HTTPS*"])
    ax.grid(color="lightgrey", axis="y")
    ax.set_axisbelow(True)
    locs = ax.get_yticks()
    labels = list(map(lambda loc: "%.0f" % np.floor(loc/1000), locs))
    ax.set_yticklabels(labels=labels)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.legend()

    plt.savefig(os.path.join(scriptpath, "BytesByModeFig.pdf"),
                bbox_inches="tight")
    plt.show()


def showBytesByFactsAndCert():
    # fig, (ax0, <ax1) = plt.subplots(ncols=2, figsize=default_figsize)
    fig0, ax0 = plt.subplots(
        figsize=[default_figsize[0]/2, default_figsize[1]])
    fig1, ax1 = plt.subplots(
        figsize=[default_figsize[0]/2, default_figsize[1]])

    ################## NUMBER OF FACTS ##################
    # Plot total bytes by mode,
    df_mod = df[df["cert_chain_len"] == 2][df["tls"]
                                           == 1][df["http"] == 1][df["stateful_mode"] == 0][df["artificial_delay"] == 0]
    agg_dict = {"bytes_total": ["mean", "std"]}

    x = df_mod.groupby("num_facts").agg("min").index
    agged = df_mod.groupby(
        "num_facts").agg(agg_dict)["bytes_total"]
    y = agged["mean"]
    yerr = 2*agged["std"]

    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    ax0.plot(x, y, "o", color="tab:blue")
    ax0.plot(x, reg(x), "--", color="tab:blue",
             label="Slope: %.0f Bytes/fact" % (coef[0]))

    ax0.legend(loc="upper left")
    ax0.set_ylim(10000, 75000)
    ax0.set_xticks(x)
    ax0.grid(color="lightgrey")
    locs = ax0.get_yticks()
    labels = list(map(lambda loc: "%.0f" % np.floor(loc/1000), locs))
    ax0.set_yticklabels(labels=labels)
    ax0.set_ylabel("Data Transmission [kB]")
    ax0.set_xlabel("Facts per contract")

    # Hide the right and top spines
    ax0.spines['right'].set_visible(False)
    ax0.spines['top'].set_visible(False)

    ################## CERTIFICATE CHAIN LENGTH ##################
    df_mod = df[df["num_facts"] == 10][df["tls"]
                                       == 1][df["http"] == 1][df["stateful_mode"] == 0]
    dfStats(df_mod)

    agg_dict = {"bytes_total": ["mean", "std"]}

    x = df_mod.groupby("cert_chain_len").agg("min").index
    agged = df_mod.groupby(
        "cert_chain_len").agg(agg_dict)["bytes_total"]
    y = agged["mean"]
    yerr = 2*agged["std"]
    print(yerr)

    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    ax1.plot(x, y, "o", color="tab:red")
    ax1.plot(x, reg(x), "--", color="tab:red",
             label="Slope: %.0f Bytes/certificate" % (coef[0]))

    ax1.legend(loc="upper left")
    ax1.set_ylim(15000, 50000)
    ax1.set_xticks(x)
    ax1.grid(color="lightgrey")
    locs = ax1.get_yticks()
    labels = list(map(lambda loc: "%.0f" % np.floor(loc/1000), locs))
    ax1.set_yticklabels(labels=labels)
    ax1.set_ylabel("Data Transmission [kB]")
    ax1.set_xlabel("Certificate chain length")

    # Hide the right and top spines
    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)

    fig0.savefig(os.path.join(scriptpath, "BytesByFacts.pdf"),
                 bbox_inches="tight")
    fig1.savefig(os.path.join(scriptpath, "BytesByCertChainLen.pdf"),
                 bbox_inches="tight")
    plt.show()


def showDelayEffects():
    # Version 1: 4 subplots with shared axes
    # Version 2: All in one plot
    _ver = 2

    df_r = df_delay
    dfStats(df_r)

    fig = plt.figure(figsize=default_figsize)
    ax_common = fig.add_subplot(111)

    if _ver == 1:
        axes = fig.subplots(
            ncols=2, nrows=2, sharex="all", sharey="all")
        ax_common.spines['top'].set_color('none')
        ax_common.spines['bottom'].set_color('none')
        ax_common.spines['left'].set_color('none')
        ax_common.spines['right'].set_color('none')
        ax_common.tick_params(labelcolor='w', top=False,
                              bottom=False, left=False, right=False)
    elif _ver == 2:
        axes = [[ax_common, ax_common], [ax_common, ax_common]]

    df_0_0 = df_r[(df_r["tls"] == 0) & (
        df_r["http"] == 0)].groupby("artificial_delay").agg(
        {"total_duration": ["mean", "std"]})
    df_0_1 = df_r[(df_r["tls"] == 1) & (
        df_r["http"] == 0)].groupby("artificial_delay").agg(
        {"total_duration": ["mean", "std"]})
    df_1_0 = df_r[(df_r["tls"] == 0) & (
        df_r["http"] == 1)].groupby("artificial_delay").agg(
        {"total_duration": ["mean", "std"]})
    df_1_1 = df_r[(df_r["tls"] == 1) & (
        df_r["http"] == 1)].groupby("artificial_delay").agg(
        {"total_duration": ["mean", "std"]})

    x = df_0_0.index
    y = df_0_0["total_duration"]["mean"]
    yerr = 2*df_0_0["total_duration"]["std"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    slope_label = "Slope: %.2f" % (coef[0])
    label = slope_label if _ver == 1 else "TCP (%s)" % slope_label
    axes[0][0].errorbar(x, y, yerr, color="tab:orange",
                        label=label)
    if _ver == 1:
        axes[0][0].set_title("TCP")

    x = df_0_1.index
    y = df_0_1["total_duration"]["mean"]
    yerr = 2*df_0_1["total_duration"]["std"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    slope_label = "Slope: %.2f" % (coef[0])
    label = slope_label if _ver == 1 else "TLS (%s)" % slope_label
    axes[0][1].errorbar(x, y, yerr, color="tab:red",
                        label=label)
    if _ver == 1:
        axes[0][1].set_title("TCP+TLS")

    x = df_1_0.index
    y = df_1_0["total_duration"]["mean"]
    yerr = 2*df_1_0["total_duration"]["std"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    slope_label = "Slope: %.2f" % (coef[0])
    label = slope_label if _ver == 1 else "HTTP (%s)" % slope_label
    axes[1][0].errorbar(x, y, yerr, color="tab:blue",
                        label=label)
    if _ver == 1:
        axes[1][0].set_title("HTTP")

    x = df_1_1.index
    y = df_1_1["total_duration"]["mean"]
    yerr = 2*df_1_1["total_duration"]["std"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    slope_label = "Slope: %.2f" % (coef[0])
    label = slope_label if _ver == 1 else "HTTPS (%s)" % slope_label
    axes[1][1].errorbar(x, y, yerr, color="tab:purple",
                        label=label)

    if _ver == 1:
        axes[1][1].set_title("HTTPS")

    # Baseline for a 3-way handshake
    if _ver == 1:
        None  # not implemented
    else:
        x = np.arange(0, np.max(df_0_0.index)+1, 1)
        ax_common.plot(x, 3*x, linestyle="dashed",
                       color="tab:green", label="Baseline (3x delay)")

    for _axes in axes:
        for ax in _axes:
            ax.legend(loc="upper left")
            ax.grid(color="lightgrey")
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.set_xlim(0)
            ax.set_ylim(0)

    if _ver == 2:
        ax_common.grid(color="lightgray")

    ax_common.set_xlabel("Artificial delay [ms]")
    ax_common.set_ylabel("Handshake duration [ms]")

    plt.savefig(os.path.join(scriptpath, "GenerationDurationByArtificialDelayFig.pdf"),
                bbox_inches="tight")
    plt.show()


# showDelayEffects()

# showBytesByMode()

showBytesByFactsAndCert()

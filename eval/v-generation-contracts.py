import os
import sys
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import matplotlib
from datetime import datetime
import dateutil

scriptpath = os.path.dirname(sys.argv[0])

df = pd.read_csv(os.path.join(scriptpath, "generation-1.csv"))
default_figsize = (9, 4)


def showByNumFacts():
    # fig, ax = plt.subplots(ncols=2, figsize=default_figsize)

    fig = []
    ax = []
    for i in range(2):
        [_fig, _ax] = plt.subplots(
            figsize=[default_figsize[0]/2, default_figsize[1]])
        fig.append(_fig)
        ax.append(_ax)

    df_r = df[df["cert_chain_len"] == 2]

    ################## PER-CONTRACT ##################
    # Plot contract sizes
    agged = df_r.groupby("num_facts").agg(
        {"contract_size": "mean"})
    x = agged.index
    y = agged["contract_size"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    ax[0].plot(x, y, "s", color="tab:blue",
               label="Contract size (Slope: %.0f Bytes/Fact)" % (coef[0]))
    ax[0].plot(x, reg(x), "--", color="tab:blue")

    min_theo = coef[0]

    ax[0].legend(loc="upper left")
    ax[0].set_ylim(5000, 30000)
    ax[0].set_xticks(x)
    ax[0].grid(color="lightgrey")
    locs = ax[0].get_yticks()
    labels = list(map(lambda loc: "%.0f" % np.floor(loc/1000), locs))
    ax[0].set_yticklabels(labels=labels)
    ax[0].set_ylabel("Contract Bytes [kB]")
    ax[0].set_xlabel("Facts per contract")

    # Hide the right and top spines
    ax[0].spines['right'].set_visible(False)
    ax[0].spines['top'].set_visible(False)

    ################## PER-FACT ##################
    x = agged.index
    y = agged["contract_size"] / agged.index
    ax[1].plot(x, y, "s", color="tab:blue")
    ax[1].plot(x, reg(x)/x, "--", color="tab:blue")

    ax[1].hlines(min_theo, 0, max(x), 'tab:red', "--",
                 "Theoretical minimum:\n%.0f Bytes/Fact" % min_theo)
    ax[1].legend(loc="upper right")
    ax[1].set_ylim(100, 10000)
    ax[1].set_yscale("log")
    ax[1].set_xticks(x)
    ax[1].grid(color="lightgrey")
    ax[1].set_yticks([100, 200, 300, 400, 500, 1000])
    locs = ax[1].get_yticks()
    labels = list(map(lambda loc: "%.0fK" % np.floor(loc/1000)
                      if loc >= 1000 else "%.0f" % loc, locs))
    ax[1].set_yticklabels(labels=labels)
    ax[1].set_ylabel("Contract Bytes/Fact")
    plt.xlabel("Facts per contract")

    # Hide the right and top spines
    ax[1].spines['right'].set_visible(False)
    ax[1].spines['top'].set_visible(False)
    ax[1].set_xlabel("Facts per contract")

    i = 0
    for _fig in fig:
        _fig.savefig(os.path.join(scriptpath, "ContractSizeByFacts-%d.pdf" % i),
                     bbox_inches="tight")
        i += 1

    plt.show()


def showByCertChainLen():
    figsize = [default_figsize[0]/2, default_figsize[1]]
    fig = plt.figure(figsize=figsize)
    ax = plt.subplot(111)

    df_r = df[df["num_facts"] == 10]
    print(df_r)

    agged = df_r.groupby("cert_chain_len").agg(
        {"contract_size": "mean"})
    x = agged.index
    y = agged["contract_size"]
    coef = np.polyfit(x, y, 1)
    reg = np.poly1d(coef)
    ax.plot(x, y, "s", color="tab:blue",
            label="Contract size (Slope:\n%.0f Bytes/certificate)" % (coef[0]))
    ax.plot(x, reg(x), "--", color="tab:blue")

    min_theo = coef[0]

    ax.legend(loc="upper left")
    ax.set_ylim(5000, 15000)
    ax.set_xticks(x)
    ax.grid(color="lightgrey")
    locs = ax.get_yticks()
    labels = list(map(lambda loc: "%.0f" % np.floor(loc/1000), locs))
    ax.set_yticklabels(labels=labels)
    ax.set_ylabel("Contract Bytes [kB]")
    ax.set_xlabel("Certificate chain length")

    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    plt.savefig(os.path.join(scriptpath, "ContractSizeByCerts.pdf"),
                bbox_inches="tight")
    plt.show()


showByNumFacts()

# showByCertChainLen()

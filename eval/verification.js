const factcheck = require("../factcheck");
const { random, util } = require("node-forge");
const fs = require("fs");
const { md5sum } = require("../lib/crypto/checksums");

const factCheckConfig = {
  useTls: false,
  clientEnforceTls: false,
  verbose: false,
  pki: {
    privateKey: { type: "rsa", format: "pem", path: "./pki/own-priv.pem" },
    ownCert: { type: "X509", format: "pem", path: "./pki/own-cert.pem" },
    caCertPath: "./pki/ca",
  },
};

const iriBase = "http://example.com/";
function makeIRI() {
  return iriBase + util.bytesToHex(random.getBytesSync(32));
}
function makeData() {
  return random.getBytesSync(1024);
}

let factStore = {};
let server;

const program = [];

if (true) {
  [10, 20, 30, 40, 50, 75, 100].forEach((factsPerContract) => {
    ["json", "jsonld-compact", "jsonld-flattened"].forEach((contractFormat) => {
      program.push({
        rounds: 30,
        addResults: true,
        contracts: 1,
        factsPerContract,
        contractFormat,
        routeDelay: 0,
      });
    });
  });
  program.push();
}

if (false) {
  [10, 20, 30, 40, 50, 75, 100].forEach((factsPerContract) => {
    ["json", "jsonld-compact", "jsonld-flattened"].forEach((contractFormat) => {
      [0, 100, 200, 300].forEach((routeDelay) => {
        [1, 10].forEach((contracts) => {
          program.push({
            rounds: 10,
            addResults: true,
            contracts,
            factsPerContract,
            contractFormat,
            routeDelay,
          });
        });
      });
    });
  });
  program.push();
}

if (true) {
  // shuffle the program
  shuffle(program);
}

function shuffle(r) {
  for (var f, n, o = r.length; 0 !== o; )
    (n = Math.floor(Math.random() * o)),
      (f = r[(o -= 1)]),
      (r[o] = r[n]),
      (r[n] = f);
  return r;
}

if (true) {
  // Empty run at the beginning to avoid unusually long first run
  // Shorter inter-round time can be tolerated, as tls and http are off
  program.unshift({
    rounds: 10,
    addResults: false,
    contracts: 10,
    contractFormat: "json",
    routeDelay: 0,
    factsPerContract: 10,
  });
}

factcheck.setConfig(factCheckConfig);
factcheck.init().then(async () => {
  factcheck.facts.addRoute({
    priority: 2,
    appliesTo: (factID) => true,
    get: (factID) => [factStore[factID], "binary"],
  });
  console.log("#### Starting verification module ####");
  for (stepCounter = 0; stepCounter < program.length; stepCounter++) {
    console.log(
      `### Starting step ${stepCounter + 1} of ${
        program.length
      } ###\nConfig:\n${JSON.stringify(program[stepCounter], null, 2)}`
    );
    await runStep(program[stepCounter]);
  }
});

async function runStep(config) {
  return new Promise((resolve) => runRound(0, config, resolve));

  async function runRound(roundIndex, config, resolve) {
    const startTime = new Date();
    const runId = md5sum(startTime);
    console.log(`## Starting round ${roundIndex + 1} of ${config.rounds} ##`);
    console.log("Generating contracts...");
    generateContracts(config).then(async (contracts) => {
      console.log("Contracts generated! Starting verification...");
      let res = await verifyContracts(contracts, config);
      res.startTime = startTime.toISOString();
      res.runId = runId;
      if (config.addResults) {
        console.log("Verification done! Adding result...");
        addResult(res, config);
      } else {
        console.log("Verification done! Adding results is disabled.");
      }
      console.log(`Round ${roundIndex + 1} of ${config.rounds} done!`);
      server.close();
      if (roundIndex < config.rounds - 1) {
        runRound(roundIndex + 1, config, resolve);
      } else {
        resolve();
      }
    });
  }
}

function generateContracts(config, then = () => {}) {
  let contracts = [];
  let onDone = () => {};
  const serverAuthID = makeIRI();
  const clientAuthID = makeIRI();
  server = factcheck.server.create(8000, {
    useTls: false,
    onError(err) {
      console.log("Server error: " + err);
    },
    contractIRICallback: (contract) => makeIRI(),
    authID: serverAuthID,
  });

  //setTimeout(requestContracts, 1000);
  requestContracts();

  function requestContracts() {
    // Reset fact store
    factStore = {};
    for (i = 0; i < config.contracts; i++) {
      // Generate facts
      let factIDs = [];
      for (j = 0; j < config.factsPerContract; j++) {
        const iri = makeIRI();
        factIDs.push(iri);
        factStore[iri] = makeData();
      }

      //Generate contract
      factcheck.client.requestContract(factIDs, {
        mode: "tcp",
        port: 8000,
        authID: clientAuthID,
        onError(err) {
          console.log("Client Error: " + err);
        },
        onContract: handleContract,
      });
    }
  }

  function handleContract(contract) {
    contracts.push(contract);
    if (contracts.length >= config.contracts) {
      onDone(contracts);
    }
  }

  return {
    then: function (thenFunc) {
      if (typeof thenFunc === "function") {
        onDone = thenFunc;
      }
    },
  };
}

async function verifyContracts(contracts, config) {
  // temporary route with delay
  const delayedRoute = {
    priority: 1,
    appliesTo: (f) => true,
    get: (factID) => {
      return new Promise((resolve) =>
        setTimeout(() => {
          resolve(factStore[factID]);
        }, config.routeDelay)
      );
    },
  };
  factcheck.facts.addRoute(delayedRoute);

  const res = {};
  res.durations = [];

  const totalStart = new Date();
  res.verification = await Promise.all(
    contracts.map(async (contract) => {
      //console.log(contract);
      let formattedContract;
      let contractFormat;
      switch (config.contractFormat) {
        case "json":
          formattedContract = contract.getContent("json");
          contractFormat = "json";
          break;
        case "jsonld-compact":
          formattedContract = await contract.getLDContent("jsonld");
          contractFormat = "jsonld";
          break;
        case "jsonld-flattened":
          formattedContract = await contract.getLDContent("jsonld-flattened");
          contractFormat = "jsonld";
          break;
      }
      const timeStarted = new Date();
      return factcheck
        .verify(formattedContract, {
          getFactMethod: "internal",
          contractFormat,
        })
        .then((verRes) => {
          const timeDone = new Date();
          res.durations.push(timeDone - timeStarted);
          return verRes;
        });
    })
  );
  const totalEnd = new Date();
  res.totalDuration = totalEnd - totalStart;

  // remove temporary route
  factcheck.facts.removeRoute(delayedRoute);
  return res;
}

const writeStream = fs.createWriteStream("./eval/verification.csv", {
  flags: "a",
});

function addResult(res, config) {
  //Only add result if all verifications succeeded
  if (
    !res.verification.every((verificationResult) => verificationResult.result)
  ) {
    console.log(
      "One of the verification results was negative! Result is not added."
    );
    console.log("Negative result:\n" + JSON.stringify(res, null, 2));
    return;
  }
  res.durations.forEach((duration) => {
    writeStream.write(
      `${res.runId},${res.startTime},${config.contractFormat},${config.contracts},` +
        `${config.factsPerContract},${config.routeDelay},${duration},` +
        `${res.totalDuration}\n`
    );
  });
}

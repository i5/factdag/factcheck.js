const { init, client, setConfig } = require("../../factcheck");
const exampleFacts = require("./example-facts.json");

const fs = require("fs");
const jsonc = require("jsonc-parser");
const path = require("path");
const config = jsonc.parse(
  fs.readFileSync(path.resolve(__dirname, "./config.jsonc")).toString()
);

setConfig(config);
const startTime = new Date();
init().then(() => {
  const afterInitTime = new Date();
  const factIDs = Object.keys(exampleFacts).filter(
    (f) => f === "example.com/facts/fact1" || Math.random() >= 0.5
  );
  const startRequestTime = new Date();
  client.requestContract(factIDs, {
    mode: "tcp",
    port: 8000,
    host: "localhost",
    onError: (err) => {
      console.log("Error requesting contract: " + err);
    },
    onContract: (contract) => {
      const finishedTime = new Date();
      console.log("Made new contract:");
      console.log(contract);
      console.info("Init took %dms", afterInitTime - startTime);
      console.info("Request took %dms", finishedTime - startRequestTime);
    },
  });
});

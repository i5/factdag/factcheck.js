const factcheck = require("../../factcheck");
const exampleFacts = require("./example-facts.json");

const fs = require("fs");
const jsonc = require("jsonc-parser");
const path = require("path");
const { md5sum } = require("../../lib/crypto/checksums");
const config = jsonc.parse(
  fs.readFileSync(path.resolve(__dirname, "./config.jsonc")).toString()
);

factcheck.setConfig(config);
factcheck.init().then(() => {
  const { server, facts } = factcheck;
  console.log("Creating server instance...");
  const serverInstance = server.create(8000, {
    useTls: true,
    onContract: handleContract,
    onError: handleError,
    contractIRICallback: (contract) => {
      // Contract IRI derived from md5 sum of contract timestamp
      return (
        "http://example.com/sender/contracts/" +
        md5sum(contract.getContent("copy").timestamp)
      );
    },
  });

  facts.addRoute({
    priority: 0,
    appliesTo: (factID) => /^example\.com/.test(factID),
    get: (factID) => exampleFacts[factID],
  });

  console.log("Server instance created! Listening...");

  function handleContract(contract) {
    console.log("New contract made:");
    console.log(contract);
  }

  function handleError(err) {
    console.log("Error creating contract:");
    console.log(err);
  }
});

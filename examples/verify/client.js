const { init, client, setConfig, verify } = require("../../factcheck");
const exampleFacts = require("./example-facts.json");

const fs = require("fs");
const jsonc = require("jsonc-parser");
const path = require("path");
const config = jsonc.parse(
  fs.readFileSync(path.resolve(__dirname, "./config.jsonc")).toString()
);

setConfig(config);
init().then(() => {
  const afterInitTime = new Date();
  const factIDs = Object.keys(exampleFacts).filter(
    (f) => f === "example.com/facts/fact1" || Math.random() >= 0.5
  );
  client.requestContract(factIDs, {
    mode: "raw",
    port: 8000,
    host: "localhost",
    onError: (err) => {
      console.log("Error requesting contract: " + err);
    },
    onContract: (contract) => {
      console.log("Made new contract! Starting verification (JSON)...");
      verify(contract.getContent("copy"), {
        getFactMethod: "direct",
        facts: exampleFacts,
        contractFormat: "object",
      }).then((res) => {
        console.log("Result:\n" + JSON.stringify(res, null, 2));
        console.log("Starting verification (JSON-LD)...");
        contract.getLDContent().then((ldContract) => {
          verify(ldContract, {
            getFactMethod: "direct",
            facts: exampleFacts,
            contractFormat: "jsonld",
          }).then((res) => {
            console.log("Result:\n" + JSON.stringify(res, null, 2));
          });
        });
      });
    },
  });
});

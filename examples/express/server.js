const http = require("http");
const https = require("https");
const express = require("express");
const factcheck = require("../../factcheck");
const exampleFacts = require("./example-facts.json");
const pki = require("../../lib/crypto/pki");

const jsonc = require("jsonc-parser");
const fs = require("fs");
const path = require("path");
const config = jsonc.parse(
  fs.readFileSync(path.resolve(__dirname, "./config.jsonc")).toString()
);
factcheck.setConfig(config);
factcheck.facts.addRoute({
  priority: 0,
  appliesTo: (factID) => /^example\.com/.test(factID),
  get: (factID) => [exampleFacts[factID]],
});
factcheck.init().then(() => {
  const factcheckMiddleware = factcheck.server.expressMiddleware({
    onError: (contract) => {
      console.log("New contract made!");
    },
    onError: (err) => {
      console.log(err);
    },
    customContentCallback: (contract) => ({
      exampleModule: "express",
      //largeData: new Array(65000).join("A"),
    }),
    contractIRICallback: (contract) => {
      // Contract IRI derived from md5 sum of contract timestamp
      return (
        "http://example.com/sender/contracts/" +
        md5sum(contract.getContent("copy").timestamp)
      );
    },
    supportStatefulMode: true,
  });

  // Setup TLS credentials
  let credentials = {
    key: pki.getPrivateKey("pem"),
    cert: pki.getCertificateChain("pem-array"),
  };

  const app = express();
  const bodyParser = require("body-parser");
  const { md5sum } = require("../../lib/crypto/checksums");
  app.use(bodyParser.json());
  app.post("/factcheckAPI", factcheckMiddleware);

  let httpServer = http.createServer(app);
  httpServer.listen(8080);

  let httpsServer = https.createServer(credentials, app);
  httpsServer.listen(8443);

  console.log("Server listening on 8080 (HTTP) and 8443 (HTTPS)...");
});

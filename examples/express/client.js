const factcheck = require("../../factcheck");
const exampleFacts = require("./example-facts.json");

const jsonc = require("jsonc-parser");
const fs = require("fs");
const path = require("path");

let mode = process.argv[2] || "http";

const config = jsonc.parse(
  fs.readFileSync(path.resolve(__dirname, "./config.jsonc")).toString()
);
factcheck.setConfig(config);
factcheck.facts.addRoute({
  priority: 0,
  appliesTo: (factID) => /^example\.com/.test(factID),
  get: (factID) => exampleFacts[factID],
});
factcheck.init().then(() => {
  const factIDs = Object.keys(exampleFacts).filter(
    (f) => f === "example.com/facts/fact1" || Math.random() >= 0.5
  );
  factcheck.client.requestContract(factIDs, {
    mode,
    port: mode === "https" ? 8443 : 8080,
    host: "localhost",
    path: "/factcheckAPI",
    onError: (err) => {
      console.log(err);
    },
    onContract: (contract) => {
      console.log("New contract made:");
      console.log(contract);
    },
    supportStatefulMode: true,
    authID: "http://example.com/receiver",
    customContent: { exampleModule: "express" },
  });
});

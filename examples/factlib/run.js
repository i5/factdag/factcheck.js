const factcheck = require("../../factcheck");
const { LdpMementoClient } = require("@i5/factlib.js");
const fs = require("fs");
const service = new LdpMementoClient();
const idFactory = service.getIDFactory();
const jsonld = require("jsonld");

const jsonc = require("jsonc-parser");
const path = require("path");
const { stdin } = require("process");
const { md5sum } = require("../../lib/crypto/checksums");

var readline = require("readline").createInterface({
  input: stdin,
});

//Import the config file. Needs to be done this way, as it is a JSONC file.
const config = jsonc.parse(
  fs.readFileSync(path.resolve(__dirname, "./config.jsonc")).toString()
);

const resourceID = idFactory.createResourceID(
  "http://localhost:8081",
  "facts/tollesbuch1"
);

async function doThings() {
  const { server, client, facts } = factcheck;
  // Add the fact route for the example facts.
  // Applies to factIDs that start with example.com
  facts.addRoute({
    priority: 0,
    appliesTo: (factID) => /^example\.com/.test(factID),
    get: (factID) => [exampleFacts[factID], "binary"],
  });

  // Add the fact route for facts using factlib.js
  // Applies to any route, but with a lower priority
  // than example facts.
  factcheck.facts.addRoute({
    priority: 5,
    appliesTo: (factID) => true, //alt: /^fact:/.test(factID),
    get: async (factID) => {
      const fID = /^fact:/.test(factID)
        ? idFactory.parseURIToFactID(factID)
        : idFactory.parseURIToResourceID(factID);
      const fact = await service.getLastFactRevision(fID);
      let newID = fact.factID.toString();
      let nQuadSerialization = fact.serialize("application/n-quads");
      let doc = await jsonld.fromRDF(nQuadSerialization, {
        format: "application/n-quads",
      });
      let canonicalSerialization = JSON.stringify(jsonld.canonize(doc));

      // Serialization: URDNA2015 (http://json-ld.github.io/normalization/spec/)
      let res = [canonicalSerialization, "URDNA2015"];
      if (factID !== newID) {
        res.push(newID);
      }
      return res;
    },
  });

  //---------- SERVER ----------

  // Create the server instance on port 8000, using TLS
  const serverInstance = server.create(8000, {
    useTls: config.useTls,
    onContract: serverHandleContract,
    onError: serverHandleError,
    contractIRICallback: contractIRIFactory,
    authID: "http://localhost:8081/.well-known/factdag-authority",
  });
  console.log("Server instance created! Listening...");

  //---------- CLIENT ----------

  showUI();

  /*factcheck.facts
    .getFactByID(
      "fact:2020-09-25T09:37:24.000Z:http://localhost:8081/facts/tollesbuch1"
    )
    .then((fact) => {
      //console.log(fact);
    });*/
}
console.log(config);

// Server callback for handling finished contracts
function serverHandleContract(contract) {
  console.log("Server: New contract made!");
}

// Server callback for handling errors during contract generation
function serverHandleError(err) {
  console.log("Server error:");
  console.log(err);
  console.log();
}

// Creating the contract IRIs based on a hash of the included timestamp
function contractIRIFactory(contract) {
  return (
    "http://example.com/bookpublisher/contracts/" +
    md5sum(contract.getContent("copy").timestamp)
  );
}

function clientHandleError(err) {
  console.log(`Client error: ${err}`);
}

function clientHandleContract(contract) {
  console.log("Client: New contract made!");
  //contract.getLDContent("jsonld-flattened").then((ldContract) => {
  //  console.log("Contract:\n" + ldContract);
  //});
  //console.log(contract);
  console.log(
    "Contract (JSON):\n" +
      JSON.stringify(contract.getContent("internal"), null, 2)
  );
  contract
    .getLDContent("jsonld")
    .then((jsonld) => console.log("Contract (JSON-LD):\n" + jsonld));
}

var factIDs;
async function showUI() {
  console.log("Choose the fact to request a contract for:");
  const authorityID = "http://localhost:8081";
  factIDs = await service
    .getDirectory(idFactory.createResourceID(authorityID, "/facts/"))
    .then((container) =>
      container.getContainedResourceList().map((fID) => fID.toString())
    );
  for (let i = 0; i < factIDs.length; i++) {
    console.log("[" + (i + 1) + "] " + factIDs[i]);
  }
}
readline.on("line", (line) => {
  factcheck.client.requestContract([factIDs[Number.parseInt(line) - 1]], {
    mode: "tcp",
    port: 8000,
    host: "localhost",
    onError: clientHandleError,
    onContract: clientHandleContract,
    customContent: {
      exampleModule: "factlib",
    },
    authID: "http://example.com/bookreader",
  });
  setTimeout(showUI, 500);
});

// Always set the config before calling init, because
// the crypto modules load the keys and certificates based on
// the config.
factcheck.setConfig(config);

// Use init().then(...) to wait for the initialization to finish first.
factcheck.init().then(doThings);

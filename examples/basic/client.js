const { init, client, facts, setConfig } = require("../../factcheck");
const exampleFacts = require("./example-facts.json");

const fs = require("fs");
const jsonc = require("jsonc-parser");
const path = require("path");
const config = jsonc.parse(
  fs.readFileSync(path.resolve(__dirname, "./config.jsonc")).toString()
);
setConfig(config);
const startTime = new Date();
init().then(() => {
  facts.addRoute({
    priority: 0,
    appliesTo: (factID) => /^example\.com/.test(factID),
    get: (factID) => [exampleFacts[factID], "string"],
  });

  const afterInitTime = new Date();
  const factIDs = Object.keys(exampleFacts).filter(
    (f) => f === "example.com/facts/fact1" || Math.random() >= 0.5
  );
  const startRequestTime = new Date();
  client.requestContract(factIDs, {
    mode: "tcp",
    port: 8000,
    host: "localhost",
    authID: "http://example.com/receiver",
    customContent: { exampleModule: "basic" },
    supportStatefulMode: true,
    onError: (err) => {
      console.log("Error requesting contract: " + err);
    },
    onContract: (contract) => {
      const finishedTime = new Date();
      console.log(
        "Contract (JSON):\n" +
          JSON.stringify(contract.getContent("internal"), null, 2)
      );
      contract
        .getLDContent("jsonld")
        .then((jsonld) => console.log("Contract (JSON-LD):\n" + jsonld));
      //console.log("ContractIRI: " + contract.contractIRI);
      //console.info("Init took %dms", afterInitTime - startTime);
      //console.info("Request took %dms", finishedTime - startRequestTime);
    },
  });
});
